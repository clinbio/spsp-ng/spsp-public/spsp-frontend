import authentication from './authentication.json'
import validation from './validation.json'
import team from './team.json'
import sample from './sample.json'
import profile from './profile.json'
import project from './project.json'
import upload from './upload.json'
import labels from './labels.json'

export default {
    authentication,
    validation,
    team,
    sample,
    profile,
    project,
    upload,
    labels
}