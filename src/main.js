import Vue from 'vue'
import App from './App'
import vuetify from './plugins/vuetify'
import router from './router'
import store from './store';
import '@mdi/font/css/materialdesignicons.css'
import './css/custom.scss'

import Snotify, {
    SnotifyPosition
} from 'vue-snotify'
import 'vue-snotify/styles/material.css'
import { LOGIN, LOGOUT, CHECK_AUTHENTICATION} from "@/store/actions.type";
import ApiService from "@/common/api.service";
import ApiServiceSPSP_Server from "@/common/api.serviceSPSP";
import ApiServiceSPSP_Loader from "@/common/api.serviceSPSPLoader";
import DateFilter from "@/common/date.filter";
import ErrorFilter from "@/common/error.filter";
import JwtService from "@/common/jwt.service";

import '@/plugins/apexcharts'

// dialogs
import MyMessageDlg from '@/components/globals/MyMessageDlg'
import MyConfirmDlg from '@/components/globals/MyConfirmDlg'
// components
import Chapter from '@/components/globals/Chapter'
import chapterButton from '@/components/globals/ChapterButton'
import ChapterMessage from '@/components/globals/ChapterMessage'
import ChapterHelp from '@/components/globals/ChapterHelp'

import MyOrganisms from '@/components/globals/MyOrganisms'
import MyOrganism from '@/components/globals/MyOrganism'
// table
import MyTableHeaderTooltip from "@/components/globals/MyTableHeaderTooltip"
import MyTableTruncatedCell from "@/components/globals/MyTableTruncatedCell"
import MyTableActionIcon from "@/components/globals/MyTableActionIcon"
// keycloak
import { cUseKeyCloackIdentification } from "@/common/config";
import { KEY_CLOAK_CLIENT_ID, KEY_CLOAK_URL, KEY_CLOAK_REALM } from "@/common/config";
import Keycloak from 'keycloak-js'
import keycloakConfig from "@/common/keycloakConfig"; 
import SPSPVue from '@/common/SPSPVue'
import SPSP from '@/common/SPSP'

Vue.config.productionTip = false;
Vue.filter("date", DateFilter);
Vue.filter("error", ErrorFilter);
//Vue.use(VueColumnsResizable);

ApiService.init();
ApiServiceSPSP_Server.init();
ApiServiceSPSP_Loader.init();

// Ensure we checked auth before each page load. If token is invalid we logout
if (cUseKeyCloackIdentification)
{
    router.beforeEach((to, from, next) =>
    {
        if (JwtService.getToken())
            Promise.all([store.dispatch(CHECK_AUTHENTICATION)])
                .then(next)
                .catch(() => store.dispatch(LOGOUT).then(() => 
                    {
                        Vue.prototype.$snotify.error("Your session has expired, you have been disconnected");
                        this.$router.push({ name: "home" })
                    }))
            next()
    });
}

Vue.use(Snotify, {
    toast: {
        position: SnotifyPosition.rightTop,
        showProgressBar: false
    }
});



Vue.config.productionTip = false

Vue.component('MyMessageDlg', MyMessageDlg)
Vue.component('myConfirmDlg', MyConfirmDlg)
Vue.component('chapter', Chapter)
Vue.component('chapterButton', chapterButton)
Vue.component('chapterMessage', ChapterMessage)
Vue.component('chapterHelp', ChapterHelp)
Vue.component('myOrganisms', MyOrganisms)
Vue.component('myOrganism', MyOrganism)
Vue.component('myTableHeaderTooltip', MyTableHeaderTooltip)
Vue.component('myTableTruncatedCell', MyTableTruncatedCell)
Vue.component('myTableActionIcon', MyTableActionIcon)

SPSPVue.initConnexionState()

Vue.prototype.$globalLoading = false;
if (cUseKeyCloackIdentification)
{
    let keycloakInitOptions = 
    {   
        url: KEY_CLOAK_URL,
        realm: KEY_CLOAK_REALM,
        clientId: KEY_CLOAK_CLIENT_ID,
        onLoad: 'login-required',
        checkLoginIframe: false  // because an ancestor violates the following Content Security Policy directive: "frame-ancestors 'self'".
    }
    var myKeycloak = new Keycloak(keycloakInitOptions);
    myKeycloak
        .init({ onLoad: keycloakInitOptions.onLoad, checkLoginIframe: keycloakInitOptions.checkLoginIframe})
        .then((auth) => {
            if (!auth) 
            {   
                window.location.reload()
                console.log( "reload")
            } else 
            {
                console.log( "Authenticated")
            }
            new Vue({
                vuetify,
                router,
                store,
                render: h => h(App)
            }).$mount('#app')
            
            let tokenDuration = myKeycloak.tokenParsed.exp - myKeycloak.tokenParsed.auth_time
            console.log( myKeycloak.tokenParsed);
            console.log( myKeycloak.tokenDuration);

            // TokenExpired / Token Refresh
            setInterval(() =>{
                if (myKeycloak.isTokenExpired( 0))
                    SPSP.logout( 'token expired', router, store)
                keycloakConfig.updateToken().then((refreshed) => {
                    if (refreshed) {
                        JwtService.saveAccessToken(myKeycloak.token);
                    } else {
                        console.log('Token not refreshed, valid for '
                            + Math.round(myKeycloak.tokenParsed.exp + myKeycloak.timeSkew - new Date().getTime() / 1000) + ' seconds');
                    }
                }).catch(() => {
                    logout( 'Failed to refresh token', router, store)
                });
            }, tokenDuration*500)
            
            myKeycloak.onTokenExpired = function (){
                keycloakConfig.updateToken( 700).then((refreshed) => {
                    if (refreshed) {
                        JwtService.saveAccessToken(myKeycloak.token);
                    } else {
                        let message = 'Token not refreshed, valid for '
                            + Math.round(myKeycloak.tokenParsed.exp + myKeycloak.timeSkew - new Date().getTime() / 1000) + ' seconds'
                        SPSP.logout(message, router, store)
                    }
                }).catch(() => {
                    SPSP.logout('Failed to refresh token', router, store)
                });
                SPSP.logout('Failed to refresh token', router, store)
            }


            myKeycloak.onAuthRefreshError = function () {
                SPSP.logout('Token refresh error', router, store)
            }

            myKeycloak.onAuthLogout = function () {
                SPSP.logout('Logout', router, store)
            }

            console.log(myKeycloak);

            Vue.axios.interceptors.request.use(
                config => 
                {
                    let token = JwtService.getAccessToken();
                    if (token) { config.headers = {'x-access-token': `${token}`}
                    }
                    return config;
                },
                error => Promise.reject(error)
            );
            Vue.$keycloak = myKeycloak // initialize before updateToken call
            keycloakConfig.updateToken();
            JwtService.saveAccessToken( myKeycloak.token);
            localStorage.setItem("vue-refresh-token", myKeycloak.refreshToken);
            let user = {}
            Object.assign(user, myKeycloak.idTokenParsed)
                store.dispatch(LOGIN, user)
                .then(()=>{

                    if (!SPSP.isValidUser(user))
                    {
                        SPSP.logout('Invalid user', router, store)
                    }
                })
                .catch(() => {})

        })
        .catch(() =>
        {
            console.log("error in keycloak")
        }) 
}
else
{
    new Vue({
        vuetify,
        router,
        store,
        render: h => h(App)
    }).$mount('#app')

    Vue.axios.interceptors.request.use(
        config => {
            let token = JwtService.getAccessToken();
            console.log( token)
            if (token) {
                config.headers = { 'x-access-token': `${token}` }
            }
            return config;
        },
        error => Promise.reject(error)
    );
    Vue.axios.interceptors.response.use(
        response => response,
        error => {
            console.log( error.response.status)
            if (error.response.status === 403 || error.response.status === 401) {
                Vue.$keycloak.logout();
                window.location.reload(true);
            }

            return Promise.reject(error);
        }
    );
   
}