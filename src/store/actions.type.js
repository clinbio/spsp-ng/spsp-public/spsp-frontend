export const FETCH_NOTIFICATIONS = "fetchNotifications";
export const NOTIFICATION_EDIT = "editNotification";
export const FETCH_PROJECTS = "fetchProjects";
export const PROJECT_CREATE = "createProject";
export const PROJECT_UPDATE = "editProject";

export const FETCH_SUMMARY = "fetchSummary";
export const FETCH_SAMPLES = "fetchSamples";
// Login and user
export const LOGIN = "login";
export const LOGOUT = "logout";
export const CHECK_AUTHENTIFICATION = "checkAuthentification"
export const UPDATE_USER = "updateUser";
export const CHECK_AUTHENTICATION = "checkAuth";
export const FETCH_MEMBERS = "fetchMembers";
export const FETCH_RIGHTS = "fetchRights";
export const MEMBER_EDIT = "editMember";
export const MEMBER_VALIDATE = "validateMember";
export const MEMBER_REJECT = "rejectMember";
// Request = Project new generation
export const FETCH_REQUESTS = "fetchRequest";
export const FETCH_REQUEST_FIELDS = "fetchRequestFields";
export const FETCH_REQUEST_RESULTS = "fetchRequestResult";
export const FETCH_REQUEST_RESULTS_COUNT = "fetchRequestResultCount";

export const FETCH_REQUEST_RESULTS_DASHBOARD = "fetchRequestResultDashBoard";
export const FETCH_REQUEST_RESULTS_DASHBOARD_COUNT = "fetchRequestResultDashBoardCount";
export const FETCH_REQUEST_RESULTS_PREVIEW = "fetchRequestResultPreview";
export const CANCEL_FETCH_REQUEST = "cancelFetchRequest";

export const FETCH_REQUEST_RESULTS_LAST_MONTH_COUNT = "fetchRequestResultLastMonthCount";
export const FETCH_REQUEST_RESULTS_LAST_MONTH = "fetchRequestResultDashBoardLastMonth"

// variant
export const FETCH_VARIANT_STATS = "fetchRequestVariantStats";
export const FETCH_VARIANT_SCORPIO = "fetchRequestVariantSexScorpio";
export const FETCH_VARIANT_PANGOLIN = "fetchRequestVariantSexPangolin"
export const FETCH_VARIANT_CANTON_VS_SCORPIO = "fetchRequestVariantCantonScorpio";
export const FETCH_VARIANT_CANTON_VS_PANGOLIN = "fetchRequestVariantCantonPangolin"

export const REQUEST_CREATE = "requestCreate";
export const REQUEST_UPDATE = "requestUpdate";
export const REQUEST_DELETE = "requestDelete";
export const FETCH_SUMMARY_REQUEST = "fetchSummaryRequest";
export const FETCH_DOWNLOAD_REQUEST_RESULTS_SUMMARY = "fetchDownloadRequestResultsSummary";
// Uploads
export const FETCH_UPLOADS = "fetchUploads";

// stats
export const FETCH_GENDER_STATS = "fetchGenderStats";
export const FETCH_AGE_STATS = "fetchAgeStats";
export const FETCH_VARIANTS_STATS = "fetchVariantStats";

// mail
export const SEND_MAIL = "sendMail";

// portal state
export const GET_PORTAL_STATUS = "getPortalStatus"
export const GET_SERVER_NEWS = "getServerNews"