import Vue from 'vue'
import Vuex from 'vuex'

import authentication from './authentication.module'
import uploads from './uploads.module'
import requests from './requests.module'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        authentication,
        uploads,
        requests
    }
});
