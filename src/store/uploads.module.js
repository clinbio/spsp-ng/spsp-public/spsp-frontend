import Vue from "vue";
import { SET_UPLOADS, RESET_STATE } from "./mutations.type";
import { FETCH_UPLOADS} from "./actions.type";
import { LoaderServiceSPSP } from "@/common/api.serviceSPSPLoader";

const initialState = {
    uploads: []
};

export const state = { ...initialState };

export const getters = {
    uploads: state => state.uploads,
    uploadsCount: (state, getters) => {
        if (getters.uploads!=null && getters.uploads.count!=null)
            return getters.uploads.count.batch
        return 0
    },
    uploadsColor: (state, getters) => {
        if (getters.uploads != null && getters.uploads.count != null)
        {
            if (getters.uploads.count.submission>getters.uploads.count.finish)
                return 'red'
            if (getters.uploads.count.waitingRaw > 0 || getters.uploads.count.waitingFasta > 0)
                return 'orange'
        }
        return 'white'
    },
    uploadsColorText: (state, getters) => {
        if (getters.uploads != null && getters.uploads.count != null) {
            if (getters.uploads.count.submission > getters.uploads.count.finish)
                return 'white'
            if (getters.uploads.count.waitingRaw > 0 || getters.uploads.count.waitingFasta > 0)
                return 'white'
        }
        return 'primary'
    }
};

export const actions = {
    async [FETCH_UPLOADS](context, upload) {
        const response = await LoaderServiceSPSP.getUploads(upload);
        context.commit(SET_UPLOADS, response?.data);
    },
    [RESET_STATE]() {
        for (let f in state) {
            Vue.set(state, f, state[f]);
        }
    }
};

const mutations = {
    [SET_UPLOADS]: (state, uploads) => (state.uploads = uploads),
};

export default {
    state,
    getters,
    actions,
    mutations
};
