// Authentification
export const SET_AUTHENTICATION = "setUser";
export const RESET_AUTHENTICATION = "logOut";
export const UPDATE_AUTHENTICATION_PROFILE = "updateProfile";
export const CHECK_AUTHENTIFICATION_DATA = "CheckAuthentificationData"
// Request/Project
export const SET_REQUESTS = "setRequest";
export const SET_REQUEST_FIELDS = "setRequestFields"; 
export const SET_REQUEST_RESULTS = "setRequestResults";
export const SET_REQUEST_RESULTS_COUNT = "setRequestResultsCount";
export const SET_REQUEST_RESULTS_DASHBOARD = "setRequestResultsDashBoard";
export const SET_REQUEST_RESULTS_DASHBOARD_COUNT = "setRequestResultsDashBoardCount";
export const SET_REQUEST_RESULTS_LAST_MONTH = "setRequestResultsLastMonth";
export const SET_REQUEST_RESULTS_LAST_MONTH_COUNT = "setRequestResultsLastMonthCount";
export const SET_REQUEST_RESULTS_PREVIEW = "setRequestResultsPreview";
export const SET_DOWNLOAD_REQUEST_RESULTS_SUMMARY = "SetDownloadRequestResultsSummary";

// variant
export const SET_VARIANT_STATS = "setVariantStats";
export const SET_VARIANT_PANGOLIN = "setRequestVariantPangolin";
export const SET_VARIANT_SCORPIO = "setRequestVariantScorpio";
export const SET_VARIANT_CANTON_VS_PANGOLIN = "setRequestVariantCantonVsPangolin";
export const SET_VARIANT_CANTON_VS_SCORPIO = "setRequestVariantCantonVsScorpio";

// request/Project
export const REQUEST_ADD = "addRequest";
export const REQUEST_REMOVE = "removeRequest";

// uploads
export const SET_UPLOADS = "setUploads";

// stats
export const SET_GENDER_STATS = "fetchGenderStats";
export const SET_AGE_STATS = "fetchAgeStats";

// Mail
export const SET_FEEDBACK_DATA = "SetFeedbackData"

// Status
export const SET_PORTAL_STATUS = "setPortalStatus"
export const SET_SERVER_NEWS = "setServerNews"

export const RESET_STATE = "resetModuleState";
export const SET_ERROR = "setError"

