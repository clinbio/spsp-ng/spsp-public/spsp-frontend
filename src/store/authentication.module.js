import Vue from 'vue'
import ApiService from "@/common/api.service";
import ApiServiceSPSP_Server from "@/common/api.serviceSPSP";
import ApiServiceSPSP_Loader from "@/common/api.serviceSPSPLoader";
import JwtService from "@/common/jwt.service";
import {LOGIN, LOGOUT, CHECK_AUTHENTICATION} from "./actions.type";
import { KEY_CLOAK_CLIENT_ID } from "@/common/config";
import { SET_AUTHENTICATION, RESET_AUTHENTICATION, SET_ERROR, UPDATE_AUTHENTICATION_PROFILE } from "./mutations.type";
import { cUseKeyCloackIdentification } from "@/common/config";
import SPSP from "@/common/SPSP.js";
import cst from '@/common/request.js';

const state = {
    errors: null,
    user: {},
    isAuthenticated: !!JwtService.getToken()
};

const getters = {
    currentUser(state) {
        return state.user;
    },
    isAuthenticated(state) {
        return state.isAuthenticated;
    },
    currentRole(state) {
        return state.user.role;
    },
    currentFunctions(state) {
        return state.user.functions;
    }
};

const actions = {
    [LOGIN](context, userCredentials) 
    {
        if (cUseKeyCloackIdentification)
        {
            const basePath = window.location.toString()
            if (!Vue.$keycloak.authenticated) 
            {
                // The page is protected and the user is not authenticated. Force a login.
                return new Promise(() => { 
                    Vue.$keycloak.login({ redirectUri: basePath.slice(0, -1) + to.path })
                        .then(() => {
                            let user = Vue.$keycloak.idTokenParsed;
                            context.commit(SET_AUTHENTICATION, user)
                            if (!SPSP.isValidUser(user))
                            {
                                //Vue.$keycloak.logout()
                            }
                        })
                        .catch(() => { context.commit(SET_ERROR, response.data); }); 
                })
            }
            else
            {
                context.commit(SET_AUTHENTICATION, userCredentials)
            }
        }
        else
        {
            return new Promise(resolve => {
                ApiService.post("authentication/signin", { username: credentials.username, password: credentials.password })
                    .then(({ data }) => { context.commit(SET_AUTHENTICATION, data); resolve(data); })
                    .catch(({ response }) => { context.commit(SET_ERROR, response.data); });        
                })
        }
    },
    [LOGOUT](context) 
    {
        if (cUseKeyCloackIdentification && Vue.$keycloak!=null) 
        {
            Vue.$keycloak.logout()
        }

        context.commit(RESET_AUTHENTICATION);
    },
    [CHECK_AUTHENTICATION](context) 
    {
        if (cUseKeyCloackIdentification) 
        {
            if (JwtService.getToken()) 
            {
                ApiServiceSPSP_Server.setHeader();
                ApiServiceSPSP_Loader.setHeader();
            } 
            else 
            {
                context.commit(RESET_AUTHENTICATION);
            }
        }
        else
        {
            if (JwtService.getToken()) 
            {
                ApiService.setHeader();
                return ApiService.get("authentication/user")
                    .then(({ data }) => { context.commit(SET_AUTHENTICATION, data);})
                    .catch((error) => { context.commit(SET_ERROR, error.data); throw error;});
            } 
            else 
            {
                context.commit(RESET_AUTHENTICATION);
            }
        }
    }
};

const mutations = {
    [SET_ERROR](state, errors) {
        state.errors = errors;
    },
    [SET_AUTHENTICATION](state, user)
    {
        if (cUseKeyCloackIdentification)
        {
            state.isAuthenticated = true
            state.user = user
            if (!_.isEmpty(user))
            {
                let groupCount = state.user.groups.length
                for (let i = 0; i < groupCount; i++) 
                {
                    let groupName = state.user.groups[i]
                    if (groupName.startsWith("LAB_"))
                    {    
                        SPSP.setLaboratoryId(state.user, groupName)
                        SPSP.setLaboratoryShortName(state.user, user.lab_acronym_key)
                        SPSP.setLaboratoryName(state.user, user.lab_name)
                    }
                    if (groupName == cst.cSPSPViewerGroup || groupName == cst.cSPSPAdminGroup || groupName == cst.cSPSPEditorGroup)
                        SPSP.setUserRole(state.user, groupName)
                }
            }
            let error = false
            if (!SPSP.isValidUser(user))
                error = true

            if (!error)
            {
                state.errors = {}
                JwtService.saveToken( JSON.stringify( state.user))
                ApiServiceSPSP_Server.setHeader()
                ApiServiceSPSP_Loader.setHeader()
            }
            else if (user.aud == KEY_CLOAK_CLIENT_ID)
            {
                state.errors = {}
                JwtService.saveToken(JSON.stringify(state.user))
                ApiServiceSPSP_Server.setHeader()
                ApiServiceSPSP_Loader.setHeader()
            }
            else
            {
                state.errors = {};
            }
            return !error
        }
        else
        {
            state.isAuthenticated = true;
            state.user = user;
            state.user.laboratory = "LAB_0003";
            state.user.group = "Group_Test";
            state.errors = {};
            JwtService.saveToken(state.user);
            ApiService.setHeader();
            return true
        }
    },
    [RESET_AUTHENTICATION](state) {
        state.isAuthenticated = false;
        state.user = {};
        state.errors = {};
        JwtService.destroyToken();
        JwtService.destroyAccessToken();
    },
    [UPDATE_AUTHENTICATION_PROFILE](state, user) {
        state.user.login = user.login;
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};
