import Vue from "vue";
import { RESET_STATE, SET_SERVER_NEWS} from './mutations.type';
import { FETCH_REQUESTS, FETCH_REQUEST_FIELDS, FETCH_REQUEST_RESULTS, REQUEST_CREATE, REQUEST_UPDATE, REQUEST_DELETE, 
    FETCH_REQUEST_RESULTS_COUNT, FETCH_REQUEST_RESULTS_DASHBOARD, FETCH_REQUEST_RESULTS_DASHBOARD_COUNT, FETCH_REQUEST_RESULTS_PREVIEW,
    FETCH_REQUEST_RESULTS_LAST_MONTH, FETCH_REQUEST_RESULTS_LAST_MONTH_COUNT, CHECK_AUTHENTIFICATION,
    FETCH_GENDER_STATS, FETCH_AGE_STATS, GET_PORTAL_STATUS, GET_SERVER_NEWS, 
    FETCH_VARIANT_SCORPIO, FETCH_VARIANT_PANGOLIN, 
    FETCH_VARIANT_CANTON_VS_SCORPIO, FETCH_VARIANT_CANTON_VS_PANGOLIN,
    FETCH_VARIANT_STATS, SEND_MAIL, FETCH_DOWNLOAD_REQUEST_RESULTS_SUMMARY
    } from './actions.type';
import { SET_REQUESTS, SET_REQUEST_FIELDS, REQUEST_ADD, SET_REQUEST_RESULTS, REQUEST_REMOVE, 
    SET_REQUEST_RESULTS_COUNT,SET_REQUEST_RESULTS_DASHBOARD, SET_REQUEST_RESULTS_DASHBOARD_COUNT, SET_REQUEST_RESULTS_LAST_MONTH, 
    SET_REQUEST_RESULTS_LAST_MONTH_COUNT, SET_REQUEST_RESULTS_PREVIEW, CHECK_AUTHENTIFICATION_DATA,
    SET_GENDER_STATS, SET_AGE_STATS, SET_PORTAL_STATUS, 
    SET_VARIANT_SCORPIO, SET_VARIANT_CANTON_VS_SCORPIO,
    SET_VARIANT_PANGOLIN, SET_VARIANT_CANTON_VS_PANGOLIN,
    SET_VARIANT_STATS, SET_FEEDBACK_DATA, SET_DOWNLOAD_REQUEST_RESULTS_SUMMARY
    } from './mutations.type';
import { QueryServiceSPSP } from '@/common/api.serviceSPSP';

// State
const initialState = {
    summary: {
        id: "",
        title: "",
        organisms: [],
        nb_of_cases_uploaded: 0
    },
    requests: [],
    requestFields:[],
    requestResults: [],
    requestResultsCount: [],
    requestResultsLastMonth: [],
    requestResultsLastMonthCount: [],
    requestResultsDashboard: [],
    requestResultsDashboardCount: [],
    requestResultsPreview:[],
    // stats
    genderStats: [],
    ageStats: [],
    // variants
    aVariantStats:[],
    aPangolin:{}, 
    aScorpio: {}, 
    aCantonVsPangolin: [],
    aCantonVsScorpio: [],
    
    authentification:"",
    requestResultsSummary:"",
    portalStatus:"",
    serverNews:{},
    feedbackInfo: ""
};

export const state = { ...initialState };

// Getters
export const getters = {
    /**
     * Get a project summmary.
     * @return {project} A specific project
     */
    requestSummary: state => state.summary,

    /**
     * Get all the requests from server.
     * @return [fields] A list of all requests
     */
    requests: state => state.requests,

    /**
     * Get authentification verification
     * @return string autentification state
     */
    authentification: state => state.authentification,

    portalStatus: state => state.portalStatus,
    serverNews: state => state.serverNews,
    feedbackInfo: state => state.feedbackInfo,

    /**
     * Get all the fields from server.
     * @return [fields] A list of all fields
     */
    requestFields: state => state.requestFields,

    /**
     * Get request results
     * @return [requestResults] A list of all requestResults
     */
    requestResults: state => state.requestResults,
    requestResultsCount: state => state.requestResultsCount, 
    requestResultsDashboard: state => state.requestResultsDashboard,
    requestResultsDashboardCount: state => state.requestResultsDashboardCount,
    requestResultsLastMonth: state => state.requestResultsLastMonth,
    requestResultsLastMonthCount: state => state.requestResultsLastMonthCount, 
    requestResultsPreview: state => state.requestResultsPreview,

    /**
     * Count all the input projects.
     * @return {integer} Number of input projects
     */
    samplesCount: getters => {
        return getters.requestResults.length
    },
    genderStats: state => state.genderStats,
    ageStats: state => state.ageStats,
    
    // variants
    aVariantStats: state => state.aVariantStats,
    aPangolin: state => state.aPangolin,
    aScorpio: state => state.aScorpio,
    aCantonVsPangolin: state => state.aCantonVsPangolin,
    aCantonVsScorpio: state => state.aCantonVsScorpio,
    requestResultsSummary: state => state.requestResultsSummary,
};

export const actions = {
    /**
     * Fetch the request fields from the API.
     * @param {context} obj
     * @return {response} Success or error.
     */
    async [FETCH_REQUESTS](context, group) {
        const response = await QueryServiceSPSP.getRequests(group);
        context.commit(SET_REQUESTS, response.data);
    },
    /**
     * Fetch the request fields from the API.
     * @param {context} obj
     * @return {response} Success or error.
     */
    async [FETCH_REQUEST_FIELDS](context) {
        const response = await QueryServiceSPSP.get_vocabulary();
        context.commit(SET_REQUEST_FIELDS, response.data);
    },

    /**
     * Fetch the gender stats from the API.
     * @param {context} obj
     * @return {response} Success or error.
     */
    async [FETCH_GENDER_STATS](context) {
        const response = await QueryServiceSPSP.getGenderStats();
        context.commit(SET_GENDER_STATS, response.data);
    },

    /**
     * Fetch the age stats from the API.
     * @param {context} obj
     * @return {response} Success or error.
     */
    async [FETCH_AGE_STATS](context) {
        const response = await QueryServiceSPSP.getAgeStats();
        context.commit(SET_AGE_STATS, response.data);
    },

    /**
     * Fetch authentification from the API.
     * @param {context} obj
     * @return {response} Success or error.
     */
    async [CHECK_AUTHENTIFICATION](context) {
        const response = await QueryServiceSPSP.checkAuthentification();
        context.commit(CHECK_AUTHENTIFICATION_DATA, response.data);
    },

    /**
     * Get portal state from the API.
     * @param {context} obj
     * @return {response} Success or error.
     */
    async [GET_PORTAL_STATUS](context) {
        const response = await QueryServiceSPSP.getPortalStatus();
        context.commit(SET_PORTAL_STATUS, response.data);
    },

    /**
     * Get news from the API.
     * @param {context} obj
     * @param {news} obj
     * @return {response} Success or error.
     */
    async [GET_SERVER_NEWS](context) {
        const response = await QueryServiceSPSP.getServerNews();
        context.commit(SET_SERVER_NEWS, response.data);
    },

    /**
      * Create a request/project
      * @param {context} obj
      * @param {request} obj
      * @return {response} Success or error.
      */
    async [REQUEST_CREATE](context, request) {
        const response = await QueryServiceSPSP.create(request);
        context.commit(REQUEST_ADD, response.data);
    },

    /**
     * Update a request/project.
     * @param {context} obj
     * @param {request} obj
     * @return {response} Success or error.
     */
    async [REQUEST_UPDATE](context, request) {
        const response = await QueryServiceSPSP.update(request);
        context.commit(REQUEST_UPDATE, response.data);
    },

    /**
     * Fetch the  query results from the API.
     * @param {context} obj
     * @param {request} obj
     * @return {response} Success or error.
     */
    async [FETCH_REQUEST_RESULTS](context, request) {
        const response = await QueryServiceSPSP.performQuery(request);
        context.commit(SET_REQUEST_RESULTS, response.data);
    },

    async [FETCH_REQUEST_RESULTS](context, request) {
        const response = await QueryServiceSPSP.performQuery(request);
        context.commit(SET_REQUEST_RESULTS, response.data);
    },

    /**
     * Fetch the count result for query results from the API.
     * @param {context} obj
     * @param {request} obj
     * @return {response} Success or error.
     */
    async [FETCH_REQUEST_RESULTS_COUNT](context, request) {
        const response = await QueryServiceSPSP.performQuery(request);
        context.commit(SET_REQUEST_RESULTS_COUNT, response.data);
    },

    /**
     * Fetch the query results for last month from the API.
     * @param {context} obj
     * @param {request} obj
     * @return {response} Success or error.
     */
    async [FETCH_REQUEST_RESULTS_LAST_MONTH](context, request) {
        const response = await QueryServiceSPSP.performQuery(request);
        context.commit(SET_REQUEST_RESULTS_LAST_MONTH, response.data);
    },

    /**
     * Fetch the count of query results for last month from the API.
     * @param {context} obj
     * @param {request} obj
     * @return {response} Success or error.
     */
    async [FETCH_REQUEST_RESULTS_LAST_MONTH_COUNT](context, request) {
        const response = await QueryServiceSPSP.performQuery(request);
        context.commit(SET_REQUEST_RESULTS_LAST_MONTH_COUNT, response.data);
    },

    /**
     * Fetch the query results for dashboard from the API.
     * @param {context} obj
     * @param {request} obj
     * @return {response} Success or error.
     */
    async [FETCH_REQUEST_RESULTS_DASHBOARD](context, request) {
        const response = await QueryServiceSPSP.performQuery(request);
        context.commit(SET_REQUEST_RESULTS_DASHBOARD, response.data);
    },

    /**
     * Fetch the query results for preview from the API.
     * @param {context} obj
     * @param {request} obj
     * @return {response} Success or error.
     */
    async [FETCH_REQUEST_RESULTS_PREVIEW](context, request) {
        const response = await QueryServiceSPSP.performQuery(request);
        context.commit(SET_REQUEST_RESULTS_PREVIEW, response.data);
    },
    /**
     * Fetch the count for query results for dashboard from the API.
     * @param {context} obj
     * @param {request} obj
     * @return {response} Success or error.
     */
    async [FETCH_REQUEST_RESULTS_DASHBOARD_COUNT](context, request) {
        const response = await QueryServiceSPSP.performQuery(request);
        context.commit(SET_REQUEST_RESULTS_DASHBOARD_COUNT, response.data);
    },
    
    /**
    * Delete an request/project.
    * @param {context} obj
    * @param {request} obj
    * @return {response} Success or error.
    */
    async [REQUEST_DELETE](context, request_name) {
        const response = await QueryServiceSPSP.delete(request_name);
        context.commit(REQUEST_REMOVE, response.data);
    },

    /**
    * Fetch the  query results from the API.
    * @param {context} obj
    * @param {request} obj
    * @return {response} Success or error.
    */
    async [FETCH_DOWNLOAD_REQUEST_RESULTS_SUMMARY](context, request) {
        const response = await QueryServiceSPSP.downloadResultSummary(request);
        context.commit(SET_DOWNLOAD_REQUEST_RESULTS_SUMMARY, response.data);
    },
    //////////////////////// VARIANTS ///////////////////////
    /**
     * Fetch variant stats from the API.
     * @param {context} obj
     * @param {parameters} obj
     * @return {response} Success or error.
     */
    async [FETCH_VARIANT_STATS](context, parameters) {
        const response = await QueryServiceSPSP.getVariantStats(parameters);
        context.commit(SET_VARIANT_STATS, response.data);
    },
    /**
     * Fetch variant pangolin from the API.
     * @param {context} obj
     * @return {response} Success or error.
     */
    async [FETCH_VARIANT_PANGOLIN](context, days) {
        const response = await QueryServiceSPSP.getVariantStats({ scorpio: false, days: days });
        context.commit(SET_VARIANT_PANGOLIN, response.data);
    },
    /**
    * Fetch variant scorpio from the API.
    * @param {context} obj
    * @param {request} obj
    * @return {response} Success or error.
    */
    async [FETCH_VARIANT_SCORPIO](context, days) {
        const response = await QueryServiceSPSP.getVariantStats({ scorpio: true, days: days });
        context.commit(SET_VARIANT_SCORPIO, response.data);
    },   
    /**
    * Fetch variant canton/pangolin from the API.
    * @param {context} obj
    * @param {request} obj
    * @return {response} Success or error.
    */
    async [FETCH_VARIANT_CANTON_VS_PANGOLIN](context, request) {
        const response = await QueryServiceSPSP.performQuery(request);
        context.commit(SET_VARIANT_CANTON_VS_PANGOLIN, response.data);
    },    
    /**
    * Fetch variant canton/scorpio from the API.
    * @param {context} obj 
    * @param {}
    * @return {response} Success or error.
    */
    async [FETCH_VARIANT_CANTON_VS_SCORPIO](context, request) {
        const response = await getVariantStats.query(request);
        context.commit(SET_VARIANT_CANTON_VS_SCORPIO, response.data);
    },

    /**
     * Send mail from the API.
     * @param {context} obj
     * @param {mail} obj
     * @return {response} Success or error.
     */
    async [SEND_MAIL](context, mail) {
        const response = await QueryServiceSPSP.sendMail( mail);
        context.commit(SET_FEEDBACK_DATA, response.data);
    },
};

export const mutations = {   
    [SET_REQUESTS]: (state, requests) => (state.requests = requests),
    [SET_REQUEST_FIELDS]: (state, requestFields) => (state.requestFields = requestFields),
    [SET_REQUEST_RESULTS]: (state, requestResults) => (state.requestResults = requestResults),
    [SET_REQUEST_RESULTS_COUNT]: (state, requestResultsCount) => (state.requestResultsCount = requestResultsCount),
    [SET_REQUEST_RESULTS_LAST_MONTH]: (state, requestResultsLastMonth) => (state.requestResultsLastMonth = requestResultsLastMonth),
    [SET_REQUEST_RESULTS_LAST_MONTH_COUNT]: (state, requestResultsLastMonthCount) => (state.requestResultsLastMonthCount = requestResultsLastMonthCount),
    [SET_REQUEST_RESULTS_DASHBOARD]: (state, requestResultsDashboard) => (state.requestResultsDashboard = requestResultsDashboard),
    [SET_REQUEST_RESULTS_DASHBOARD_COUNT]: (state, requestResultsDashboardCount) => (state.requestResultsDashboardCount = requestResultsDashboardCount),
    [SET_REQUEST_RESULTS_PREVIEW]: (state, requestResultsPreview) => (state.requestResultsPreview = requestResultsPreview),
    [REQUEST_ADD]: (state, requests) => (state.requests.unshift(requests)),
    [REQUEST_REMOVE]: (state, requests) => (state.requests.unshift( requests)),
    [CHECK_AUTHENTIFICATION_DATA]: (state, authentification) => (state.authentification = authentification),
    [SET_DOWNLOAD_REQUEST_RESULTS_SUMMARY]: (state, requestResultsSummary) => (state.requestResultsSummary = requestResultsSummary),
    
    // stats
    [SET_GENDER_STATS]: (state, genderStats) => (state.genderStats = genderStats),
    [SET_AGE_STATS]: (state, ageStats) => (state.ageStats = ageStats),
    // variants
    [SET_VARIANT_STATS]: (state, aVariantStats) => (state.aSexVsPangolin = aVariantStats),
    [SET_VARIANT_PANGOLIN]: (state, aPangolin) => (state.aPangolin = aPangolin),
    [SET_VARIANT_SCORPIO]: (state, aScorpio) => (state.aScorpio = aScorpio),
    [SET_VARIANT_CANTON_VS_PANGOLIN]: (state, aCantonVsPangolin) => (state.aCantonVsPangolin = aCantonVsPangolin),
    [SET_VARIANT_CANTON_VS_SCORPIO]: (state, aCantonVsScorpio) => (state.aCantonVsScorpio = aCantonVsScorpio),
    // status
    [SET_PORTAL_STATUS]: (state, portalStatus) => (state.portalStatus = portalStatus),
    [SET_SERVER_NEWS]: (state, serverNews) => (state.serverNews = serverNews),
    // mail
    [SET_FEEDBACK_DATA]: (state, feedbackInfo) => (state.feedbackInfo = feedbackInfo),

    [RESET_STATE]() {
        for (let f in state) {
            Vue.set(state, f, state[f]);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
