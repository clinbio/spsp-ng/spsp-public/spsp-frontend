import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import JwtService from "@/common/jwt.service";
import { API_URL } from "@/common/config";

const ApiService = {
    init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = API_URL;
    },

    setHeader() {
        Vue.axios.defaults.headers.common[
            "Authorization"
        ] = `Bearer ${JwtService.getToken()}`;
    },

    query(resource, params) {
        return Vue.axios.get(resource, params).catch(error => {
            throw new Error(`[RWV] ApiService ${error}`);
        });
    },

    get(resource, id = null) {
        return Vue.axios.get(`${resource}${id != null ? "/"+id : ''}`, { signal: spsp_server_controller.signal }).catch(error => {
            throw new Error(`[RWV] ApiServiceSPSP_Server ${error}`);
        });
    },

    post(resource, params) {
        return Vue.axios.post(`${resource}`, params);
    },

    update(resource, id, params, action = '') {
        return Vue.axios.patch(`${resource}/${id}${action != "" ? '/' + action : ''}`, params);
    },

    put(resource, params) {
        return Vue.axios.put(`${resource}`, params);
    },

    delete(resource) {
        return Vue.axios.delete(resource).catch(error => {
            throw new Error(`[RWV] ApiService ${error}`);
        });
    }
};

export default ApiService;

export const QueryService = {
    get() {
        return ApiService.get("users/input_projects");
    },
    find(id) {
        if (typeof id !== "number") {
            throw new Error(
                "[RWV] ProjectsService.find() project id required to retrieve data"
            );
        }
        return ApiService.get('projects', id);
    },
    create(project) {
        return ApiService.post("projects/input_project/create", {
            title: project.title,
            description: project.description
        });
    },
    update(project) {
        if (typeof project.id !== "number") {
            throw new Error(
                "[RWV] ProjectsService.update() project id required to update profile"
            );
        }
        return ApiService.update('projects', project.id, {
            "title": project.title,
            "description": project.description,
        }, 'update_project');
    },
    samples(id) {
        if (typeof id !== "number") {
            throw new Error(
                "[RWV] ProjectsService.find() project id required to retrieve data"
            );
        }
        return ApiService.get(`/projects/${id}/samples`);
    }
};

/*export const NotificationsService = {
    get() {
        return ApiService.get("users/notifications");
    },
    update(id, params) {
        if (typeof id !== "number") {
            throw new Error(
                "[RWV] NotificationsService.update() notification id required to update status"
            );
        }
        return ApiService.update("notifications", id, { status: params }, "update_status");
    }
};
*/
export const ProjectsService = {
    get() {
        return ApiService.get("users/input_projects");
    },
    find(id) {
        if (typeof id !== "number") {
            throw new Error(
                "[RWV] ProjectsService.find() project id required to retrieve data"
            );
        }
        return ApiService.get('projects', id);
    },
    create(project) {
        return ApiService.post("projects/input_project/create", {
            title: project.title,
            description: project.description
        });
    },
    update(project) {
        if (typeof project.id !== "number") {
            throw new Error(
                "[RWV] ProjectsService.update() project id required to update profile"
            );
        }
        return ApiService.update('projects', project.id, {
            "title": project.title,
            "description": project.description,
        }, 'update_project');
    },
    samples(id) {
        if (typeof id !== "number") {
            throw new Error(
                "[RWV] ProjectsService.find() project id required to retrieve data"
            );
        }
        return ApiService.get(`/projects/${id}/samples`);
    }
};

/*export const DataWatchersService = {
    get() {
        return ApiService.get("users/monitor_projects");
    },
    create(dataWatcher) {
        return ApiService.post("projects/monitor_project/create", {
            title: dataWatcher.title,
            description: dataWatcher.description,
            access_level: dataWatcher.target,
            query: dataWatcher.query
        });
    }
};
*/

export const MetadataService = {
    query(params) {
        if (typeof params.accessLevel !== "string") {
            throw new Error(
                "[RWV] MetadataService.query() access level required to retrieve metadata"
            );
        }
        return ApiService.query('/cv_metadata_types/summary', { params: { accessLevel: params.accessLevel, is_queryable: true } });
    }
};

export const SamplesService = {
    get() {
        return ApiService.get("/users/samples_to_review");
    },
    validate(sample) {
        if (typeof sample.id !== "number") {
            throw new Error(
                "[RWV] SamplesService.validate() sample id required to validate sample"
            );
        }
        return ApiService.update('samples', sample.id, sample, 'validate');
    }
};

export const SpeciesService = {
    get() {
        return ApiService.get("/samples/mlst_by_species");
    }
};

export const ProfileService = {
    update(user) {
        if (typeof user.id !== "number") {
            throw new Error(
                "[RWV] ProfileService.update() user id required to update profile"
            );
        }
        return ApiService.update('users', user.id, {
            "login": user.login,
            "old_password": user.old_password,
            "new_password": user.new_password,
        }, 'update_password');
    }
}
