import axios from "axios";
import Vue from 'vue';
import { API_URL_SPSP_PORTAL } from "@/common/configSPSP";
import { cUseKeyCloackIdentification } from "@/common/config";
import JwtService from "@/common/jwt.service";

const spsp_loader_controller = new AbortController();
const spsp_loader_axios = axios.create({
    baseURL: API_URL_SPSP_PORTAL
});

/*spsp_loader_axios.interceptors.request.use(
    config => {
        let token = JwtService.getAccessToken();
        console.log( token)
        if (token) {
            config.headers = { 'x-access-token': `${token}` }
        }
        return config;
    },
    error => Promise.reject(error)
);*/

spsp_loader_axios.interceptors.response.use(
    (response) => { return response;},
    (error) => 
    {
        if (error.response.status === 403 || error.response.status === 401) 
        {
            if (cUseKeyCloackIdentification && Vue.$keycloak!=null) 
            {
                Vue.$keycloak.logout()
            }
            return Promise.reject(error);
        }
        return error;
    }
);


const ApiServiceSPSP_Loader = {
    init() {
    },

    setHeader() {
        if (cUseKeyCloackIdentification) {
            spsp_loader_axios.defaults.headers.common[
                "Authorization"
            ] = `Bearer ${JwtService.getAccessToken()}`;
        }
        else {
            Vue.axios.defaults.headers.common[
                "Authorization"
            ] = `Bearer ${JwtService.getToken()}`;
        }
    },

    getWithParams(resource, params) {
        const paramsText = new URLSearchParams(params)
        return spsp_loader_axios.get(`${resource}?${paramsText.toString()}`, { signal: spsp_loader_controller.signal})
            .catch(error => { throw new Error(`[RWV] ApiServiceSPSP_Loader ${error}`);
        });
    }
};

export default ApiServiceSPSP_Loader

export const LoaderServiceSPSP = {
    getUploads(upload) {
        return ApiServiceSPSP_Loader.getWithParams("loader_report", { days: upload.days/3});
    },
    cancelRequest()
    {
        spsp_loader_controller.abort()
    }
};
