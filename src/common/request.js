import Vue from 'vue'
import Misc from "@/common/misc.js"
import { cUseKeyCloackIdentification } from "@/common/config";
import SPSP from "@/common/SPSP"

const cSPSPViewerGroup = "SPSP Viewer"
const cSPSPAdminGroup = "SPSP Admin"
const cSPSPEditorGroup = "SPSP Editor"

const cDefaultSamplesItemPerPage = 15;
const cDefaultSamplesItemPerLongPage = 50;
const cEnumType = "ENUM";
const cDateType = "DATE";
const cStringType = "STRING";
const cIntType = "INT";
const cFloatType = "FLOAT";
const cBoolType = "BOOL";
const cNumberType = "NUMBER";

const cOR = "OR";
const cAND = "AND";
const cIN = "IN";
const cEQUAL = "=";
const cNOT_EQUAL = "!=";
const cLESS = "<";
const cGREATER = ">";
const cLESS_EQUAL = "<=";
const cGREATER_EQUAL = ">=";

const cFieldStart = "";
const cFieldEnd = "";
const cRangeStart = "{";
const cRangeEnd = "}";
const cParenthesisStart = "(";
const cParenthesisEnd = ")";

function isIntValid(value)
{
    return Misc.getIntRegex().test(value) && !isNaN(parseInt(value)) && parseInt(value).toString() === value
}

function isFloatValid(value)
{
    return Misc.getFloatRegex().test(value) && !isNaN(parseFloat(value)) /*&& parseFloat( value).toString()===value  */
}

function isTypeOf(field, typeStr)
{
    if (field == null)
        return false
    return field.type === typeStr
}

function isRange( operator)
{
    if (operator == null)
        return false
    return operator == cIN
}

function isEnum(field)
{
    return isTypeOf(field, cEnumType)
}

function isInt(field)
{
    return isTypeOf(field, cIntType)
}

function isFloat(field)
{
    return (isTypeOf(field, cFloatType) || isTypeOf(field, cNumberType))
}

function isBool(field)
{
    return isTypeOf(field, cBoolType)
}

function isDate(field)
{
    return isTypeOf(field, cDateType)
}

function isString(field)
{
    return isTypeOf(field, cStringType)
}

function isNotEmpty( value)
{
    return value != ""
}
function isStringValid(value) 
{
    return value != null && value != ""
}

function isBoolValid(value) {
    return value != null
}

function isDateValid(value) {
    return (value != null || value != "")
}
function getComparisonOperatorForType( field)
{
    if (this.isEnum(field))
        return [cEQUAL, cIN, cNOT_EQUAL]
    else if (this.isInt(field) || this.isDate(field) || this.isFloat(field))
        return [cEQUAL, cLESS, cGREATER, cGREATER_EQUAL, cLESS_EQUAL, cIN, cNOT_EQUAL]
    else if (this.isString(field) || this.isBool(field))
        return [cEQUAL, cNOT_EQUAL]
}

function getRangeText(value1, value2) {
    return " " + cRangeStart + " " + value1 + " , " + value2 + cRangeEnd
}

function writeFieldToText(field, operator, context)
{
    if (field === null)
        return
    let isRange = this.isRange(operator) 
    let fieldText = cFieldStart + field.name + cFieldEnd
    if (this.isEnum(field)) {
        if (isRange)
            return fieldText + " " + cIN + " " + cRangeStart + " " + context.enumSelectedItemList + cRangeEnd
        else
            return fieldText + " = " + context.enumSelectedItem
    }
    else if (this.isDate(field)) {
        if (isRange)
            return fieldText + " " + operator + this.getRangeText(context.firstDate, context.secondDate)
        else
            return fieldText + " " + operator + context.firstDate
    }
    else if (this.isInt(field)) {
        if (isRange)
            return fieldText + " " + operator + this.getRangeText(context.firstInt, context.secondInt)
        else
            return fieldText + " " + operator + " " + context.firstInt
    }
    else if (this.isFloat(field)) {
        if (isRange)
            return fieldText + " " + operator + this.getRangeText(context.firstNumber, context.secondNumber)
        else
            return fieldText + " " + operator + " " + context.firstNumber
    }
    else if (this.isString(field)) {
        return fieldText + " = " + "'" + context.textValue + "'"
    }
    else if (this.isBool(field)) {
        return fieldText + " " + operator + context.boolValue
    }
}

function isFieldPropertiesValid(field, operator, context)
{
    let isValid = false
    if (operator=='')
        return false
    let isRange = this.isRange(operator)
    if (this.isEnum(field)) {
        if (isRange) {
            if (context.enumSelectedItemList.length > 0)
                isValid = true
        }
        else if (context.enumSelectedItem !== "")
            isValid = true
    }
    else if (this.isInt(field)) {
        isValid = this.isIntValid(context.firstInt)
        if (isValid && isRange)
            isValid = this.isIntValid(context.secondInt)
    }
    else if (this.isFloat(field)) {
        isValid = this.isFloatValid(context.firstNumber)
        if (isValid && isRange)
            isValid = this.isFloatValid(context.secondNumber)
    }
    else if (this.isDate(field)) {
        isValid = this.isDateValid(context.firstDate)
        if (isValid && isRange)
            isValid = this.isDateValid(context.secondDate)
    }
    else if (this.isString(field))
        isValid = this.isStringValid(context.textValue)
    else if (this.isBool(field))
        isValid = this.isBoolValid(context.boolValue)
    return isValid
}

/**
 * @vuese
 * Used to check the function of the current user
 * @arg String The function to be checked (FIXME should be a list)
 */
function checkUserFunction( user, functionToCheck) 
{
    return (
        user.functions &&  user.functions.includes(functionToCheck)
    );
}

/**
 * @vuese
 * Used to check the permissions of the current user
 * @arg Object The user to be checked
 * @arg String The permission to be checked (FIXME should be a list)
 */
function checkUserPermission(user, permission) 
{
    return (
        user.permissions && user.permissions.includes(permission)
    );
}

/**
 * @vuese
 * Used to check the permissions of the current user
  * @arg Object The user to be checked
*/
function userCanEditProject(user) 
{
    if (cUseKeyCloackIdentification)
    {
        return (SPSP.getUserRole(user) == cSPSPAdminGroup) || (SPSP.getUserRole(user) == cSPSPEditorGroup)
    }
    else
    {
        return checkUserPermission( user, 'EDIT_SMART_PROJECT') || checkUserPermission( user,'EDIT_LAB_INPUT_PROJECT')
    }
}
/**
 * @vuese
 * Used to check the permissions of the current user
  * @arg Object The user to be checked
*/
function userCanViewProject(user) {
    if (cUseKeyCloackIdentification) 
    {
        return (SPSP.getUserRole(user) == cSPSPAdminGroup) || (SPSP.getUserRole(user) == cSPSPEditorGroup) || (SPSP.getUserRole(user) == this.cSPSPViewerGroup) 
    }
    else 
    {
        return checkUserPermission( user, 'EDIT_SMART_PROJECT') || checkUserPermission( user, 'EDIT_LAB_INPUT_PROJECT')
    }
}

/**
 * @vuese
 * Used to check the permissions of the current user
  * @arg Object The user to be checked
*/
function userCanImportProject(user) {
    if (cUseKeyCloackIdentification) {
        return (SPSP.getUserRole(user) == cSPSPAdminGroup)
    }
    else {
        return false
    }
}
/**
 * @vuese
 * Used to check the permissions of the current user
  * @arg Object The user to be checked
*/
function userCanViewUpload(user)
{
    if (cUseKeyCloackIdentification) 
    {
        return (SPSP.getUserRole(user) == cSPSPEditorGroup) || (SPSP.getUserRole(user) == cSPSPAdminGroup) || (SPSP.getUserRole(user) ==cSPSPViewerGroup) 
    }
    else 
    {
        return cst.checkUserPermission( user, 'DATA_SUBMISSION')
    }
}

/**
 * @vuese
 * Used to check the permissions of the current user
  * @arg Object The user to be checked
*/
function userCanManageMembers(user) 
{
    if (cUseKeyCloackIdentification) 
    {
        if (Vue.$keycloak == null)
            return false
        return Vue.$keycloak.hasResourceRole(cSPSPAdminGroup)
    }
    else
        return checkUserPermission( user, 'CONFIRM_USER')
}

/**
 * @vuese
 * Used to get submission criteria for request
  * @arg int number of days
*/
function getSumbissionDateCriteria( daysCnt)
{
   return ("submission.batch_submission_date>TODAY-" + daysCnt)
}

/**
 * @vuese
*/
function getAllIsolationDateCriteria()
{
    return "isolate.isolation_date <= TODAY"
}

export default
{  
    cAND,
    cOR,
    cRangeStart,
    cRangeEnd,
    cParenthesisStart,
    cParenthesisEnd,

    cDefaultSamplesItemPerPage,
    cDefaultSamplesItemPerLongPage,
    cSPSPViewerGroup,
    cSPSPAdminGroup,
    cSPSPEditorGroup,

    isIntValid,
    isFloatValid,
    isRange,
    isEnum,
    isInt,
    isFloat,
    isBool,
    isDate,
    isString,
    isNotEmpty,
    isStringValid,
    isBoolValid,
    isFieldPropertiesValid,
    isDateValid,
    writeFieldToText,
    getRangeText,
    getComparisonOperatorForType,

    getAllIsolationDateCriteria,
    getSumbissionDateCriteria,

    // user rights
    checkUserFunction,
    checkUserPermission,
    userCanEditProject,
    userCanViewProject,
    userCanViewUpload,
    userCanManageMembers,
    userCanImportProject
}