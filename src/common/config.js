export const API_URL = process.env.VUE_APP_API;          // Check .env file
export const APP_DEBUG = process.env.VUE_APP_DEBUG;       // Check .env file
export const MODE = process.env.VUE_APP_MODE;            // Check .env file
export const INSTANCE = process.env.VUE_APP_INSTANCE;    // Check .env file
export const APP_VERSION = process.env.VUE_APP_VERSION;  // Check package.json
// KEY_CLOAK
export const KEY_CLOAK_CLIENT_ID = process.env.VUE_APP_KEY_CLOAK_CLIENT; // Check .env file
export const KEY_CLOAK_URL = process.env.VUE_APP_KEY_CLOAK_URL; // Check .env file
export const KEY_CLOAK_REALM = process.env.VUE_APP_KEY_CLOAK_REALM; // Check .env file


// SITE
export const SITE_TITLE = "Swiss Pathogen Surveillance Platform";
export const SITE_URL = process.env.BASE_URL;
export const SITE_PATHOGEN = "SARS-CoV-2";
export const SITE_SHORTNAME = "SPSP";
export const SITE_DESCRIPTION = "The Swiss Pathogen Surveillance Platform connects human and veterinary microbiology laboratories across Switzerland to enable shared surveillance and outbreak analyses based on whole genome sequencing data and associated epidemiological and clinical data.";
export const SITE_EMAIL_SUPPORT = "spsp-support@sib.swiss";
export const SITE_ABOUT = "https://spsp.ch";
export const SITE_DOCUMENTATION = "https://spsp.ch";

export const cLoadDataFromFile = false;
export const cUseKeyCloackIdentification = true;
export const cUploadPeriodInDays =900;


export default { 
    API_URL, 
    APP_VERSION,
    APP_DEBUG,
    KEY_CLOAK_CLIENT_ID,
    KEY_CLOAK_URL,
    KEY_CLOAK_REALM,
    MODE, 
    SITE_TITLE,
    INSTANCE, 
    SITE_SHORTNAME, 
    SITE_DESCRIPTION,
    SITE_PATHOGEN, 
    cLoadDataFromFile, 
    cUseKeyCloackIdentification,
    cUploadPeriodInDays
}
