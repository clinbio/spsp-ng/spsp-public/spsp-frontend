const cIsolationAllLaboratoriesByPeriod    = 1
const cIsolationCantonsByPeriod = 2
const cIsolationByCantonByPeriod = 4 
const cIsolationByLaboratoryByPeriod = 32
const cIsolationByLaboratories = 64
const cIsolationByLineageByPeriod = 128
const cIsolationMyLaboratoryByPeriod = 256
const cSubmissionMyLaboratoryByPeriod = 1
const cSubmissionCantonsByPeriod = 2
const cSubmissionAllLaboratoriesByPeriod = 4
const cSequencingPurpose = 1
const cTATForAll = 2
const cTATForMyLaboratory = 4
const cTATByCanton = 8
// NextClade
const cNextCladeQCStatus = 16
const cNextCladeQCStatusByLineage = 32
const cNextCladeQCMissing = 64
const cNextCladeQCMissingByLineage = 128

import Misc from "@/common/misc"
import SPSP from "@/common/SPSP"
import MyDate from "@/common/myDate"


function parseIsolationSamplesData( currentUser, samplesDataTable, dataToParse, period, start, results)
{
    if (_.isEmpty(samplesDataTable))
    {
        return false;
    }

    let size = Math.min(samplesDataTable.maxIndex + 1, samplesDataTable.lastIndex)
    let extractIsolationAllLaboratoriesByPeriod = Misc.checkFlag(dataToParse, cIsolationAllLaboratoriesByPeriod)
    let extractIsolationCantonsByPeriod = Misc.checkFlag(dataToParse, cIsolationCantonsByPeriod)
    let extractIsolationByCantonByPeriod = Misc.checkFlag(dataToParse, cIsolationByCantonByPeriod)
    let extractIsolationByLaboratoryByPeriod = Misc.checkFlag(dataToParse, cIsolationByLaboratoryByPeriod)
    let extractIsolationByLineageByPeriod = Misc.checkFlag(dataToParse, cIsolationByLineageByPeriod)
    let extractIsolationLaboratoriesByPeriod = Misc.checkFlag(dataToParse, cIsolationByLaboratories)
    let extractIsolationMyLaboratoryByPeriod = Misc.checkFlag(dataToParse, cIsolationMyLaboratoryByPeriod)
    
    var dateIsolation, isMyLaboratory, laboratory, canton
    let currentLab = SPSP.getLaboratoryName(currentUser)
    let aIsolationCanton = new Set()
    let aIsolationLaboratory = new Set()
    let aIsolationLineage = new Set()
    let currentDate = MyDate.getCurrentDate()

    if (size>=1)
        results.startingDate = samplesDataTable.rows[1][0]

    for (let i = 0; i < size; i++)
    {
        // dates
        dateIsolation = samplesDataTable.rows[i][0]
        laboratory = samplesDataTable.rows[i][2]
        canton = samplesDataTable.rows[i][3]
        isMyLaboratory = (laboratory == currentLab);

        if (canton == SPSP.cDataEmptyStr)
            canton = SPSP.cUnknownStr

        let inSwitzerland = (samplesDataTable.rows[i][4] == "SWITZERLAND")

        //  ----------------------------------------------------------
        //  ------------------- isolation ---------------------------
        //  ----------------------------------------------------------
        if (extractIsolationAllLaboratoriesByPeriod)
        {
            Misc.countByKey( results.aIsolationDatesCount, dateIsolation)
        }
        
        // count by canton in switzerland
        if (extractIsolationCantonsByPeriod)
        {
            if (inSwitzerland) 
            {
                if (MyDate.getDiffDateInDays(dateIsolation, currentDate)<=30)
                {
                    Misc.countByKey(results.aIsolationDatesByCantonLastMonth, canton)
                    if (isMyLaboratory)                   
                        Misc.countByKey(results.aMyIsolationDatesByCantonLastMonth, canton)
                }
                Misc.countByKey(results.aIsolationDatesByCanton, canton)
                if (isMyLaboratory)
                    Misc.countByKey(results.aMyIsolationDatesByCanton, canton)
            }
        }

        // date count by lab
        if (extractIsolationMyLaboratoryByPeriod && isMyLaboratory)
        {
            Misc.countByKey(results.aIsolationDatesCountForMyLaboratory, dateIsolation)        
        }
        if (extractIsolationByLaboratoryByPeriod) 
        {
            if (inSwitzerland) 
            {
                Misc.countByTwoKey(results.aIsolationByDateByLaboratory, MyDate.getPeriod(dateIsolation, period, start), laboratory)
                aIsolationLaboratory.add(laboratory)
            }
        }
        if (extractIsolationLaboratoriesByPeriod)
        {    
            if (inSwitzerland) 
            {
                let laboratoryName = laboratory
                if (laboratoryName == SPSP.cEmptyLabStr)
                    laboratoryName = SPSP.cOtherLabsStr
                if (isMyLaboratory)
                    Misc.countByKey(results.aMyIsolationDatesByLaboratory, laboratoryName)

                if (MyDate.getDiffDateInDays(dateIsolation, currentDate) <= 30)
                    Misc.countByKey(results.aIsolationDatesByLaboratoryLastMonth, laboratoryName)

                Misc.countByKey(results.aIsolationDatesByLaboratory, laboratoryName)
            }
        }    
        // date count for by lineage
        if (extractIsolationByLineageByPeriod) 
        {
            if (inSwitzerland) 
            {
                Misc.countByTwoKey(results.aIsolationByDateByLineage, MyDate.getPeriod(dateIsolation, period, start), samplesDataTable.rows[i][5])
                aIsolationLineage.add(samplesDataTable.rows[i][5])
            }
        }
        
        // count by canton by day in switzerland
        if (extractIsolationByCantonByPeriod) {
            if (inSwitzerland)
            {
                Misc.countByTwoKey(results.aIsolationByDateByCanton, MyDate.getPeriod(dateIsolation, period, start), canton)
                aIsolationCanton.add(canton)
            }
        }
    }

    if (aIsolationCanton.size > 0) {
        results.aIsolationCanton = [...aIsolationCanton]
        Misc.completeCountByTwoKey(results.aIsolationByDateByCanton, results.aIsolationCanton)
    }

    if (aIsolationLaboratory.size > 0) {
        results.aIsolationLaboratory = [...aIsolationLaboratory]
        results.aIsolationLaboratory.sort()
        Misc.completeCountByTwoKey(results.aIsolationByDateByLaboratory, results.aIsolationLaboratory)
    }
    if (aIsolationLineage.size > 0) {
        results.aIsolationLineage = [...aIsolationLineage]
        results.aIsolationLineage.sort()
        Misc.completeCountByTwoKey(results.aIsolationByDateByLineage, results.aIsolationLineage)
    }
    return true;
}
    

function parseSubmissionSamplesData(currentUser, samplesDataTable, dataToParse, period, start, results) {

    if (_.isEmpty(samplesDataTable))
    {
        return false;
    }
    let size = Math.min(samplesDataTable.maxIndex + 1, samplesDataTable.lastIndex)

    let extractSubmissionAllLaboratoriesByPeriod = Misc.checkFlag(dataToParse, cSubmissionAllLaboratoriesByPeriod)
    let extractSubmissionMyLaboratoryByPeriod = Misc.checkFlag(dataToParse, cSubmissionMyLaboratoryByPeriod)
    let extractSubmissionCantonsByPeriod = Misc.checkFlag(dataToParse, cSubmissionCantonsByPeriod)

    var dateSubmission, laboratory, canton, isMyLaboratory

    let currentLab = SPSP.getLaboratoryName(currentUser)
 
    if (size >= 1)
        results.startingDate = samplesDataTable.rows[1][0]

    for (let i = 0; i < size; i++) {
        // dates
        dateSubmission = samplesDataTable.rows[i][1]
        laboratory = samplesDataTable.rows[i][2]
        canton = samplesDataTable.rows[i][3]
        isMyLaboratory = (laboratory == currentLab)
 
        if (canton == SPSP.cDataEmptyStr)
            canton = SPSP.cUnknownStr

        let inSwitzerland = (samplesDataTable.rows[i][4] == "SWITZERLAND")


        //  ----------------------------------------------------------
        //  ------------------- submission ---------------------------
        //  ----------------------------------------------------------
        if (extractSubmissionAllLaboratoriesByPeriod)
            if (inSwitzerland)
                Misc.countByKey(results.aSubmissionDatesCount, dateSubmission)

        // date count for my lab
        if (extractSubmissionMyLaboratoryByPeriod && isMyLaboratory)
        {
            Misc.countByKey(results.aSubmissionDatesCountForMyLaboratory, dateSubmission)
        }

        // count by canton in switzerland
        if (extractSubmissionCantonsByPeriod) {
            if (inSwitzerland)
                Misc.countByKey(results.aSubmissionDatesByCanton, canton)
        }
    }
    return true;
}

function parseTATSamplesData(currentUser, samplesDataTable, dataToParse, period, start, results) {
    let size = Math.min(samplesDataTable.maxIndex + 1, samplesDataTable.lastIndex)

    let extractTATForMyLaboratory = Misc.checkFlag(dataToParse, cTATForMyLaboratory)
    let extractTATForAll = Misc.checkFlag(dataToParse, cTATForAll)
    let extractTATByCanton = Misc.checkFlag(dataToParse, cTATByCanton)

    var dateIsolation, dateSubmission, laboratory, canton, isMyLaboratory
    let currentLab = SPSP.getLaboratoryName(currentUser)
    let currentDate = MyDate.getCurrentDate()

    if (size >= 1)
        results.startingDate = samplesDataTable.rows[1][0]

    for (let i = 0; i < size; i++) {
        // dates
        dateIsolation = samplesDataTable.rows[i][0]
        dateSubmission = samplesDataTable.rows[i][1]
        laboratory = samplesDataTable.rows[i][2]
        canton = samplesDataTable.rows[i][3]
        isMyLaboratory = (laboratory == currentLab)

        if (canton == SPSP.cDataEmptyStr)
            canton = SPSP.cUnknownStr

        let inSwitzerland = (samplesDataTable.rows[i][4] == "SWITZERLAND")

        //  ----------------------------------------------------------
        //  ------------------- TAT------- ---------------------------
        //  ----------------------------------------------------------
        if (extractTATForMyLaboratory && isMyLaboratory)
            Misc.aggregateByKey(results.aTATForMyLaboratory, MyDate.getPeriod(dateIsolation, period, start), MyDate.getDiffDateInDays(dateIsolation, dateSubmission))

        if (extractTATForAll)
            Misc.aggregateByKey(results.aTATForAll, MyDate.getPeriod(dateIsolation, period, start), MyDate.getDiffDateInDays(dateIsolation, dateSubmission))

        if (extractTATByCanton) {
            if (inSwitzerland && MyDate.getDiffDateInDays(dateIsolation, currentDate) <= 60) {
                Misc.aggregateByKey(results.aTATByCantons, canton, MyDate.getDiffDateInDays(dateIsolation, dateSubmission))
            }
        }
    }
}

function getNextClassMissingStatus( missingCnt)
{
    let ratio = missingCnt / 3000
    let status = SPSP.cNextCladeGoodMissing
    if (ratio >= 1)
        status == SPSP.cNextCladeBadMissing
    else if (ratio > 0.1)
        status = SPSP.cNextCladeMediumMissing
    return status
}


function parseNextCladeSamplesData(currentUser, samplesDataTable, dataToParse, period, start, results) {
    let size = Math.min(samplesDataTable.maxIndex + 1, samplesDataTable.lastIndex)
    
    let extractNextCladeQCStatus = Misc.checkFlag(dataToParse, cNextCladeQCStatus)
    let extractNextCladeQCStatusByLineage = Misc.checkFlag(dataToParse, cNextCladeQCStatusByLineage)
    let extractNextCladeQCMissing = Misc.checkFlag(dataToParse, cNextCladeQCMissing)
    let extractNextCladeQCMissingByLineage = Misc.checkFlag(dataToParse, cNextCladeQCMissingByLineage)

    var dateIsolation, laboratory, canton, isMyLaboratory
    let currentLab = SPSP.getLaboratoryName(currentUser)

    if (size >= 1)
        results.startingDate = samplesDataTable.rows[1][0]

    for (let i = 0; i < size; i++) {
        // dates
        dateIsolation = samplesDataTable.rows[i][0]
        laboratory = samplesDataTable.rows[i][2]
        canton = samplesDataTable.rows[i][3]
        isMyLaboratory = (laboratory == currentLab)
        if (canton == SPSP.cDataEmptyStr)
            canton = SPSP.cUnknownStr

        //  ----------------------------------------------------------
        //  ------------------- NextClade- ---------------------------
        //  ----------------------------------------------------------
        if (extractNextCladeQCStatus) {
            if (isMyLaboratory) {
                Misc.countByTwoKey(results.aNextCladeQCStatus, MyDate.getPeriod(dateIsolation, period, start), samplesDataTable.rows[i][6])
            }
        }
        if (extractNextCladeQCStatusByLineage) {
            if (isMyLaboratory)
                Misc.countByThreeKey(results.aNextCladeQCStatusByLineage, MyDate.getPeriod(dateIsolation, period, start), samplesDataTable.rows[i][6], samplesDataTable.rows[i][5])
        }

        if (extractNextCladeQCMissing) {
            if (isMyLaboratory) {
                Misc.countByTwoKey(results.aNextCladeQCMissing, MyDate.getMonthEnd(dateIsolation), getNextClassMissingStatus(parseInt(samplesDataTable.rows[i][7])))
            }
        }

        if (extractNextCladeQCMissingByLineage) {
            if (isMyLaboratory) {
                Misc.countByThreeKey(results.aNextCladeQCMissingByLineage, MyDate.getPeriod(dateIsolation, period, start), getNextClassMissingStatus(parseInt(samplesDataTable.rows[i][7])), samplesDataTable.rows[i][5])
            }
        }
    }
}

export default {
    parseIsolationSamplesData,
    parseSubmissionSamplesData,
    parseNextCladeSamplesData,
    parseTATSamplesData,

    cIsolationAllLaboratoriesByPeriod,
    cIsolationCantonsByPeriod,
    cIsolationMyLaboratoryByPeriod,
    cSubmissionAllLaboratoriesByPeriod,
    cSubmissionMyLaboratoryByPeriod,
    cSubmissionCantonsByPeriod,
    cIsolationByCantonByPeriod,
    cIsolationByLaboratoryByPeriod,
    cIsolationByLineageByPeriod,
    cIsolationByLaboratories,
    cSequencingPurpose,
    cTATForAll,
    cTATForMyLaboratory,
    cTATByCanton,
    cNextCladeQCStatus,
    cNextCladeQCStatusByLineage,
    cNextCladeQCMissing,
    cNextCladeQCMissingByLineage,
 }