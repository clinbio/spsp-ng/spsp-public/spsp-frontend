import Vue from 'vue';
import axios from "axios";
import { API_URL_SPSP_PORTAL} from "@/common/configSPSP";
import { cUseKeyCloackIdentification } from "@/common/config";
import JwtService from "@/common/jwt.service";

const spsp_server_controller = new AbortController();
const spsp_server_axios = axios.create({
    baseURL: API_URL_SPSP_PORTAL
});

/*spsp_server_axios.interceptors.request.use(
    config => {
        let token = JwtService.getAccessToken();
        console.log( token)
        if (token) {
            config.headers = { 'x-access-token': `${token}` }
        }
        return config;
    },
    error => Promise.reject(error)
);*/

spsp_server_axios.interceptors.response.use(
    (response) => { return response;},
    (error) => 
    {
        console.log( "Error in axios", error.response)
        if (error.response.status === 403 || error.response.status === 401) 
        {
            if (cUseKeyCloackIdentification && Vue.$keycloak!=null) 
            {
                Vue.$keycloak.logout()
            }
            return Promise.reject(error);
        }
        return error;
    }
);


const ApiServiceSPSP_Server = {
    init() {},

    setHeader() {
        if (cUseKeyCloackIdentification) {
            spsp_server_axios.defaults.headers.common[
                "Authorization"
            ] = `Bearer ${JwtService.getAccessToken() }`;
        }
        else {
            Vue.axios.defaults.headers.common[
                "Authorization"
            ] = `Bearer ${JwtService.getToken()}`;
        }
    },

    getWithParams(resource, params) {
        const paramsText = new URLSearchParams(params)
        return spsp_server_axios.get(`${resource}?${paramsText.toString()}`, { signal: spsp_server_controller.signal }).catch(error => {
            throw new Error(`[RWV] ApiServiceSPSP_Server ${error}`);
        });
    },

    get(resource, id = null) {
        return spsp_server_axios.get(`${resource}${id != null ? "/"+id : ''}`, { signal: spsp_server_controller.signal }).catch(error => {
            throw new Error(`[RWV] ApiServiceSPSP_Server ${error}`);
        });
    },

    post(resource, params) {
        return spsp_server_axios.post(`${resource}`, params, { signal: spsp_server_controller.signal }).catch(error => {
            throw new Error(`[RWV] ApiServiceSPSP_Server ${error}`);
        });
    },
}

export const QueryServiceSPSP = {
    get_vocabulary() {
        return ApiServiceSPSP_Server.get("portal_query_vocabulary");
    },
    checkAuthentification() {
        return ApiServiceSPSP_Server.get("auth");
    }, 
    getPortalStatus() {
        return ApiServiceSPSP_Server.get("isRunning");
    },
    sendMail( mail){
        return ApiServiceSPSP_Server.post("mail", mail);
    },
    performQuery( query) {
        return ApiServiceSPSP_Server.post("portal_query", {
            columns: query.columns,
            sortOrder: query.sortOrder,
            criteria: query.criteria,
            firstIndex: query.firstIndex,
            lastIndex: query.lastIndex
        });
    },
    downloadResultSummary(query) {
        return ApiServiceSPSP_Server.post("portal_download", {
            columns: query.columns,
            sortOrder: query.sortOrder,
            criteria: query.criteria,
            firstIndex: query.firstIndex,
            lastIndex: query.lastIndex
        });
    },
    create(query) {
        return ApiServiceSPSP_Server.post("query_save", {
            name: query.name,
            description: query.description,
            columns: query.columns,
            criteria: query.criteria,
            sortOrder: query.sortOrder
        });
    },
    getRequests( ) {
        return ApiServiceSPSP_Server.get("query_filter");
    },
    delete(query) {
        return ApiServiceSPSP_Server.post("query_delete", {
            name: query.name
        });
    },
    update(query) {
        return ApiServiceSPSP_Server.post('query_rename', { 
            name: query.name, 
            newName: query.newName
        });
    },
    cancelRequest() {
        spsp_server_controller.abort()
    },
    getGenderStats() {
        return ApiServiceSPSP_Server.get("gender_stat");
    },
    getAgeStats() {
        return ApiServiceSPSP_Server.get("age_stat");
    },
    getVariantStats( parameters) {
        return ApiServiceSPSP_Server.getWithParams("lineage_stat", { scorpio: parameters.scorpio, days: parameters.days });
    },
    getServerNews() {
        return ApiServiceSPSP_Server.get("news");
    },
};


export default ApiServiceSPSP_Server