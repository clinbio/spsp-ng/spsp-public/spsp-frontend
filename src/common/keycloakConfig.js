import Vue from 'vue';

async function updateToken() {
    await Vue.$keycloak.updateToken(700);
    return Vue.$keycloak.token;
  }

export default 
{
    updateToken
}
