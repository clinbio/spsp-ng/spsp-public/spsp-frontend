
const cDayPeriod = Symbol("day")
const cWeekPeriod = Symbol("week")
const cMonthPeriod = Symbol("month")
const cNoPeriod = Symbol("-")

function getWeekStart(date) {
  var offset = new Date(date).getDay();
  return new Date(new Date(date) - offset * 24 * 60 * 60 * 1000)
    .toISOString()
    .slice(0, 10);
}

function getWeekEnd(date) {
  var offset = 6 - new Date(date).getDay();
  return new Date(new Date(date) + offset * 24 * 60 * 60 * 1000)
    .toISOString()
    .slice(0, 10);
}

function getMonthStart(dateStr) {
  var date = new Date(dateStr), y = date.getFullYear(), m = date.getMonth();
  return new Date(new Date(y, m, 1))
    .toISOString()
    .slice(0, 10);
}

function getMonthEnd(dateStr) {
  var date = new Date(dateStr), y = date.getFullYear(), m = date.getMonth();
  return new Date(new Date(y, m + 1, 1))
    .toISOString()
    .slice(0, 10);
}

function getDiffDateInDays(dateStr1, dateStr2) {
  var date1 = new Date(dateStr1);
  var date2 = new Date(dateStr2);

  // To calculate the no. of days between two dates
  return (date2.getTime() - date1.getTime()) / (1000 * 3600 * 24);
}

function getCurrentDate() {
  var today = new Date();
  return today.getFullYear() + '-' + (today.getMonth() + 1).toString().padStart(2, "0") + '-' + today.getDate().toString().padStart(2, "0");
}

function getCurrentMonth() {
  var today = new Date();
  return today.getFullYear() + '-' + (today.getMonth() + 1).toString().padStart(2, "0");
}

function getStartingDate( ellapsedDays) {
  var startingDate = new Date();
  startingDate.setDate(startingDate.getDate() - ellapsedDays);
  return startingDate.toLocaleDateString()
 }

function moveDate( date, ellapsedDays) {
  var newDate = new Date();
  newDate.setTime(date.getTime() + ellapsedDays * 1000 * 3600 * 24);
  return newDate.getFullYear() + '-' + (newDate.getMonth() + 1).toString().padStart(2, "0") + '-' + newDate.getDate().toString().padStart(2, "0");
}

function getPeriod(dateStr, period, start)
{
    if (period == cWeekPeriod)
      return (start ? getWeekStart(dateStr) : getWeekEnd(dateStr))
    else if (period==cMonthPeriod)
      return (start ? getMonthStart(dateStr) : getMonthEnd(dateStr))

    return new Date(dateStr).toISOString().slice(0, 10);       
}

export default {
    cDayPeriod,
    cWeekPeriod,
    cMonthPeriod,
    cNoPeriod,
    getDiffDateInDays,
    getCurrentDate,
    getCurrentMonth,
    getStartingDate,
    moveDate,
    getPeriod,
    getWeekStart,
    getWeekEnd,
    getMonthStart,
    getMonthEnd
}