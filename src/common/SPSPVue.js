//import Vue from 'vue'
import store from '../store'
function initConnexionState()
{
  /*Vue.prototype.$portalConnexionSucceed = false;
  Vue.prototype.$loaderConnexionSucceed = false;
  Vue.prototype.$serverConnexionSucceed = false;
  */  
  store.state.portalConnexionSucceed = false;
  store.state.loaderConnexionSucceed = false;
  store.state.serverConnexionSucceed = false;
}

function isPortalResponding() {
  return (store.state.portalConnexionSucceed != false);
}

function isLoaderResponding()
{
  return store.state.loaderConnexionSucceed != false
}

function isServerResponding()
{
  return store.state.serverConnexionSucceed != false
}

function setPortalResponding( state) {
  store.state.portalConnexionSucceed = state
}

function setLoaderResponding( state) {
  store.state.loaderConnexionSucceed = state
}

function setServerResponding( state) {
  store.state.serverConnexionSucceed = state
}

function displayConnexionStatus()
{
  /*console.log(store.state.serverConnexionSucceed)
  console.log(store.state.portalConnexionSucceed)
  console.log(store.state.loaderConnexionSucceed)*/
}

function getAllRequestName( aRequests)
{
  let aExistingName = [];
  if (aRequests==null)
    return aExistingName;

  for (let i = 0; i < aRequests.length; i++)
    aExistingName.push(aRequests[i].name);
  return aExistingName
}

function getNonExistingName(name, aExistingName) {
  let index = 1;
  let newName = name;
  for (; ;) 
  {
    if (aExistingName.findIndex(item => item == newName) < 0)
      return newName;
    newName = name + index;
    index++;
  }
}

export default {
  setPortalResponding,
  setLoaderResponding,
  setServerResponding,

  getNonExistingName,
  getAllRequestName,

  isPortalResponding,
  isLoaderResponding,
  isServerResponding,

  initConnexionState,
  displayConnexionStatus
}
   