const aDefaultColors =
    [   '#008FFB','#00E396', '#FF9800', '#E91E63', '#9C27B0', '#CED4DC', '#673AB7', '#3F51B5', '#2196F3', 
        '#F44336', '#4CAF50', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEB3B', '#FFC107', 
        '#FF5722', '#795548', '#607D8B', '#9E9E9E']
const cSemaphorColors = ["#00FF7F", '#FFFF00', '#FF0000']

import Misc from "@/common/misc"

function getColorsForGraphics(
    colorCnt
) 
{
    var aColors = []
    let defaultColorCnt = aDefaultColors.length
    for (let i = 0; i < colorCnt; i++)
        aColors.push(aDefaultColors[i % defaultColorCnt])
    return aColors
}
function getColorForGraphics(
    colorIndex
) {
    var aColors = []
    let defaultColorCnt = aDefaultColors.length
    aColors.push(aDefaultColors[colorIndex % defaultColorCnt])
    return aColors
}


function addConvertTimelineToSeries(seriesOutput, dates, serieName, cumulative, convertDate)
{
    if (dates==null)
        return
    var serie = {}
    let data = []
    serie["name"] = serieName
    //dates.sort( function(a, b) { return Date( a) - Date( b); }) fails to sort
    //dates.sort( Misc.compareKeyDate)  fails to sort
    const ordered = Misc.sortByKey(dates)
    let aKeys = Misc.getKeys(ordered)
    let aValues = Misc.getValues(ordered)
    let size = aKeys.length
    let cumulativeValue = 0;
    for (let i = 0; i < size; i++) 
    {
        var rObj = {}
        if( convertDate)
            rObj['x'] = new Date(aKeys[i]).getTime()
        else
            rObj['x'] = aKeys[i]
        if (cumulative) 
        {
            rObj['y'] = aValues[i] + cumulativeValue
            cumulativeValue += aValues[i]
        }
        else
            rObj['y'] = aValues[i]
        data.push(rObj)
    }
    serie["data"] = data
    seriesOutput.push(serie)
    if (size>=1)
        return aKeys[ 0]
    return "" 
}

function addConvertTimelineToSeries2(seriesOutput, dates, serieName, cumulative) {
    let aData = []
    //dates.sort( function(a, b) { return Date( a) - Date( b); }) fails to sort
    //dates.sort( Misc.compareKeyDate)  fails to sort
    const ordered = Misc.sortByKey(dates)
    let aKeys = Misc.getKeys(ordered)
    let aValues = Misc.getValues(ordered)
    let size = aKeys.length
    let cumulativeValue = 0;
    for (let i = 0; i < size; i++) {
       if (cumulative) {
           aData.push([new Date(aKeys[i]).getTime(), cumulativeValue]);
            cumulativeValue += aValues[i]
        }
        else
           aData.push([new Date(aKeys[i]).getTime(), aValues[i]]);
    }
    seriesOutput.push({ name: serieName, data: aData})
    if (size >= 1)
        return aKeys[0]
    return ""
}

function convertArrayToSeries(values, alabels)
{
  let aKeys = Object.keys(values)
  let aValues = Object.values(values)
  let size = aKeys.length
  let aValuesInArray = []
  for (let i = 0; i < size; i++) 
  {
    aValuesInArray.push(aValues[i])
    alabels.push(aKeys[i])
  }
  return aValuesInArray
}

function getAgeRangeStart(
    ageStr
    )
{
    let age = parseInt(ageStr)
    if ( age>=80)
        return '80+'
    else
        return (Math.trunc(age / 10) * 10).toString() +"-" +((Math.trunc(age / 10)+1) * 10-1).toString()
}

function addTitle(graphOptions, titleText, align)
{
    graphOptions.title = {
        text: titleText,
        align: align
    }
}

function histogram_GetOptions(graphHeight, titleText, aColors, aCategories)
{
    let graphOptions = 
    {
        chart: 
        {
            type: 'bar',
            height: graphHeight,
            events: { click: function (/*chart, w, e*/) { /*console.log(chart, w, e)*/ } }
        },
        colors: aColors,
        plotOptions: {
            bar: { columnWidth: '45%', distributed: true  }
        },
        dataLabels: { enabled: false },
        legend: { show: false },
        xaxis: {
            categories: aCategories,
            labels: {
                style: { colors: aColors, fontSize: '12px' }
            }
        },
        tooltip: {
            x: {
                show: true,
                format: 'dd MMM yyyy',
                formatter: undefined,
            },
            formatter: function (val) { return val }
        },
        noData: { text: "No data to display" }
    }
    addTitle(graphOptions, titleText, 'center')
    return graphOptions
}

function histogram_GetOptions1(graphId, graphHeight, stacked, percent, aCategories) 
{
    var options = {
        chart: {
            id: graphId,
            type: 'bar',
            height: graphHeight,
            toolbar: { show: true },
            zoom: { enabled: true }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false,
                borderRadius: 10
            },
        },
        xaxis: {
            type: 'datetime',
            categories: aCategories,
        },
        legend: {
            position: 'right',
            offsetY: 40
        },
        tooltip: {
            x: {
                show: true,
                format: 'dd MMM yyyy',
                formatter: undefined,
            },
            formatter: function (val) { return val} 
        },
        fill: { opacity: 1 },
        noData: { text: "No data to display" }
    }
    if (stacked) 
        options.chart.stacked = true
    if (percent>0)
        options.chart.stackType = percent + '%'
    return options
}

function histogram_GetOptionsForCategory1(graphId, graphHeight, stacked, percent, aCategories) {
    var options = {
        chart: {
            id: graphId,
            type: 'bar',
            height: graphHeight,
            toolbar: { show: true },
            zoom: { enabled: true }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false,
                borderRadius: 10
            },
        },
        xaxis: {
            type: 'category',
            categories: aCategories,
        },
        legend: {
            position: 'right',
            offsetY: 40
        },
        tooltip: 
        { 
            x: {
                show: true,
                format: 'dd MMM yyyy',
                formatter: undefined,
            },
            formatter: function (val) { return val } 
        },
        fill: { opacity: 1 },
        noData:{ text: "No data to display"}
    }
    if (stacked)
        options.chart.stacked = true
    if (percent > 0)
        options.chart.stackType = percent + '%'
    return options
}

function areaChart_GetOptions( graphHeight, titleText, aColors)
{
    let graphOptions = {
        chart: 
        {
            type: 'area',
            height: graphHeight,
            stacked: true,
            events: {
                selection: function (/*chart, e*/) { /*console.log(new Date(e.xaxis.min))*/ }
            },
        },
        colors: aColors,
        dataLabels: { enabled: false },
        stroke: { curve: 'smooth' },
        fill: {
            type: 'gradient',
            gradient: {
                opacityFrom: 0.6,
                opacityTo: 0.8,
            }
        },
        /* TEST HIDE 0 Value to move tooltip:*/
        tooltip:{
            x: {
                show: true,
                format: 'dd MMM yyyy',
                formatter: undefined,
            },
            y: {
                formatter: function (value, /*{ series, seriesIndex, dataPointIndex, w}*/) 
                {
                    if (value=="0")
                        return "-"
                    return value
                }
            }
        },
        legend: { position: 'top', horizontalAlign: 'left' },
        xaxis: { type: 'datetime' },
        noData: { text: "No data to display" }

    }
    addTitle(graphOptions, titleText, 'center')
    return graphOptions
}

function areaChart_GetOptionsWithId(graphId, graphHeight, titleText, aColors) {
    let graphOptions = areaChart_GetOptions(graphHeight, titleText, aColors)
    graphOptions.id = graphId
    return graphOptions
}

function multipleYChart_GetOptions( graphHeight, titleText, aXCategories)
{
    let graphOptions = {
        chart:
        {
            type: 'line',
            height: graphHeight,
            stacked: false
        },
        dataLabels: { enabled: false },
        stroke: { width: [1, 1, 4] },

        xaxis: { categories: aXCategories },
        yaxis: [],
            tooltip: {
            fixed: {
                enabled: true,
                position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
                offsetY: 30,
                offsetX: 60
            },
        },
        legend: { horizontalAlign: 'left', offsetX: 40 },
        noData: { text: "No data to display" }
    }
    addTitle(graphOptions, titleText, 'center')
    graphOptions.title.offsetX = 110
    return graphOptions
}

function pieChart_GetOptions( aLabels, monochrome, height)
{

    return {
        chart: {
            toolbar: { show: true },
            height: height,
            type: 'donut',
        },
        labels: aLabels,
        theme: {
            monochrome: { enabled: monochrome }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                chart: { width: 250 },
                legend: { show: false }
            }
        }],
        legend: {
            position: 'right',
            offsetY: 0,
            height: 230,
        },
        noData: { text: "No data to display" }
    }
}

function pieChart_GetOptionsWithId( graphId, aLabels, monochrome, chartWidth) {
    let graphOptions = pieChart_GetOptions(aLabels, monochrome, chartWidth)
    graphOptions.id = graphId
    return graphOptions
}

function multipleYChart_AddYAxis(graphOptions, serieName, titleText, titleColor, right)
{
    graphOptions.yaxis.push(
        {
            seriesName: serieName,
            opposite: right,
            axisTicks: { show: true },
            axisBorder: { show: true, color: titleColor },
            labels: { style: { colors: titleColor } },
            title: {
                text: titleText,
                style: { color: titleColor }
            },
            tooltip: { enabled: true }
        })
}

function multipleYChart_AddYSerie(aSeries, serieName, serieType, colorData, serieData)
{
    aSeries.push({ name: serieName, type: serieType, data: serieData, color: colorData})
}

function boxPlot_AddSeries(aSeries, aBoxPlotData, legend)
{
    aSeries.push({
        name: legend,
        type: 'boxPlot',
        data: aBoxPlotData
    })
}

function boxPlot_AddData(aBoxPlotData, xStr, value) 
{
    aBoxPlotData.push({ x: xStr, y: value })
} 
function boxPlot_AddDateData(aBoxPlotData, dateStr, value)
{
    aBoxPlotData.push({ x: new Date(dateStr).getTime(), y: value })
}

function boxPlot_AddDatePoint(aBoxPlotPoints, dateStr, value)
{
    aBoxPlotPoints.push({ x: new Date(dateStr).getTime(), y: value })
}

function boxPlot_AddPoint(aBoxPlotPoints, xStr, value) {
    aBoxPlotPoints.push({x: xStr,  y:value})
}

function boxPlot_AddPoints(aSeries, aBoxPlotPointsData, legend)
{
    aSeries.push({
        name: legend,
        data: aBoxPlotPointsData,
        type: 'scatter'
    })
}

function boxPlot_GetOptionsForDate( graphId, graphHeight, aColors) 
{
    let chartOptions =
    {
        chart:
        {
            id: graphId,
            type: 'boxPlot',
            height: graphHeight
        },
        colors: aColors,
        xaxis: {
            type: 'datetime',
            tooltip: { formatter: function (val) { return new Date(val).getFullYear() }}
        },
        tooltip: { shared: false, intersect: true },
        noData: { text: "No data to display" }
    }
    return chartOptions
}

function boxPlot_GetOptionsForCategoryAsDate(graphHeight, aColors, aCategories) 
{
    let chartOptions =
    {
        chart:
        {
            type: 'boxPlot',
            height: graphHeight
        },
        colors: aColors,
        xaxis: {
            type: 'datetime',
            categories: aCategories,
            tickAmount: aCategories.length+1,
            labels: {
                formatter: function (val) 
                {
                    let date = new Date(val)
                    //if (date.getMonth()<=0)
                        return aCategories[ date.getFullYear()-2000]
                    /*else
                        return ""*/
                },
            }
        },
        tooltip: { shared: false, intersect: true },
        noData: { text: "No data to display" }
    }
    return chartOptions
}

function boxPlot_GetOptionsForCategory(graphHeight, aColors, aCategories) 
{
    let chartOptions =
    {
        chart:
        {
            type: 'boxPlot',
            height: graphHeight
        },
        colors: aColors,
        xaxis: {
            type: 'category',
            categories: aCategories,
            labels: {
                formatter: function (val) { return val},
            }
        },
        tooltip: { shared: false, intersect: true },
        noData: { text: "No data to display" }
    }
    return chartOptions
}

function boxPlot_ComputeValues(data) {
    var aBoxValues = []
    let Q1 = Misc.getPercentile(data, 25)
    let Q3 = Misc.getPercentile(data, 75)
    let max = Math.max.apply(Math, data)
    let min = Math.min.apply(Math, data)
    aBoxValues.push(Math.max(min, Q1 - 1.5 * (Q3 - Q1)))
    aBoxValues.push(Q1)
    aBoxValues.push(Misc.getPercentile(data, 50))
    aBoxValues.push(Q3)
    aBoxValues.push(Math.min(max, Q3 + 1.5 * (Q3 - Q1)))
    return aBoxValues
}

function addSeries( graphId, nameText, aData)
{
    ApexCharts.exec(graphId, 'appendSeries', { name: nameText, data: aData})
}

function addBoxPlotSeries(graphId, nameText, aData) {
    ApexCharts.exec(graphId, 'appendSeries', { name: nameText, type: 'boxPlot', data: aData })
}
function toggleSeries(graphId, nameText/*, state*/) 
{
    ApexCharts.exec(graphId, toggleSeries, { nameText })
}

function showSeries(graphId, nameText, state) {
    if (state)
        ApexCharts.exec(graphId, 'showSeries', { nameText })
    else
        ApexCharts.exec(graphId, 'hideSeries', { nameText })
}  

function heatMap_GetOptionsMulticolorsMatrix(graphId, graphHeight, aColors)
{
    let chartOptions =
    {
        chart:
        {
            id: graphId,
            type: 'heatmap',
            height: graphHeight
        },
        plotOptions: {
            heatmap:
            {
                shadeIntensity: 0.5,
                radius: 0,
                useFillColorAsStroke: true,
                colorScale: { ranges: aColors }
            }
        },
        dataLabels: { enabled: false },
        stroke: { width: 1 },
        noData: { text: "No data to display" }
    }
    return chartOptions
}

function heatMap_GetOptionMulticolors(graphId, graphHeight, aColors, colorAreHorizontal) 
{
    let chartOptions =
    {
        chart:
        {
            id: graphId,
            type: 'heatmap',
            height: graphHeight,
        },
        plotOptions: {
            heatmap:
            {
                colorScale: { inverse: colorAreHorizontal }
            }
        },
        dataLabels: { enabled: false },
        colors: aColors,
        noData: { text: "No data to display" }
    }
    return chartOptions
}

function heatMap_GetOptionsMonochromeMatrix(graphId, graphHeight, color) {
    let chartOptions =
    {
        chart:
        {
            id: graphId,
            type: 'heatmap',
            height: graphHeight,
        },
        dataLabels: { enabled: false },
        colors: [color],
        noData: { text: "No data to display" }
    }
    return chartOptions
}


function setXAxisCategories( chartId, aCategories) 
{
    //chartOptions.xaxis = { type:'category', categories: aCategories} /*fails*/
    ApexCharts.exec(chartId, 'updateOptions', { xaxis: { type: 'category', categories: aCategories } })
}
function setXAxisCategories2(chartOptions, aCategories) {
    chartOptions.xaxis = { type: 'category', categories: aCategories } /*fails*/
}
function setYAxisCategories(chartId, aCategories) {
    // chartOptions.yaxis = { type:'category', categories: aCategories} fails*/
    ApexCharts.exec(chartId, 'updateOptions', { yaxis: { type: 'category', categories: aCategories } })
}

function setDateTimeXAxisCategories(chartId, aDates) {
    // chartOptions.xaxis = { type:'category', categories: aCategories} fails*/
    ApexCharts.exec(chartId, 'updateOptions', { xaxis: { type: 'dateTime', categories: aDates } })
}

function updateColors(chartId, aColors) {
    // chartOptions.xaxis = { type:'category', categories: aCategories} fails*/
    ApexCharts.exec(chartId, 'updateOptions', { colors: aColors})
}

/*function heatMap_SetYAxisCategories(chartId, aCategories) {
    ApexCharts.exec(chartId, 'updateOptions', { yaxis: { type: 'category', categories: aCategories } })
}*/

export default {
    cSemaphorColors,
    addConvertTimelineToSeries,
    addConvertTimelineToSeries2,
    convertArrayToSeries,
    addTitle,
    getColorForGraphics,
    getColorsForGraphics,
    getAgeRangeStart,
    // xAxis 
    setXAxisCategories,
    setXAxisCategories2,
    setYAxisCategories,
    setDateTimeXAxisCategories,
    updateColors,
    // serie
    addSeries,
    showSeries,
    toggleSeries,
    addBoxPlotSeries,

    // histogram
    histogram_GetOptions,
    histogram_GetOptions1,
    histogram_GetOptionsForCategory1,
    // multiple
    multipleYChart_GetOptions,
    multipleYChart_AddYAxis,
    multipleYChart_AddYSerie,
    // area chart
    areaChart_GetOptions,
    areaChart_GetOptionsWithId,
    pieChart_GetOptions,
    pieChart_GetOptionsWithId,

    // box plot
    boxPlot_GetOptionsForDate,
    boxPlot_GetOptionsForCategory,
    boxPlot_GetOptionsForCategoryAsDate,
    boxPlot_AddPoints,
    boxPlot_AddPoint, 
    boxPlot_AddDatePoint,
    boxPlot_AddDateData,
    boxPlot_AddData,
    boxPlot_AddSeries,
    boxPlot_ComputeValues,

    // heat map
    heatMap_GetOptionsMulticolorsMatrix,
    heatMap_GetOptionsMonochromeMatrix,
    heatMap_GetOptionMulticolors,
}