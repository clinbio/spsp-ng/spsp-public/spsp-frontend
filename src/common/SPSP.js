import Misc from '@/common/misc'
import MyDate from '@/common/myDate'
import { APP_DEBUG } from "@/common/config.js" 
import {  LOGOUT } from "@/store/actions.type";

const cLastMonthStr = 'Last month'
const cLastWeekStr = 'Last week'
const cLastYearStr = 'Last year'
const cFullPeriodStr = 'All'
const cUnknownStr = 'UNKNOWN'
const cEmptyLineage = 'UNASSIGNED'
const cDataEmptyStr = '-'
const cEmptyLabStr = '-'
const cOtherLabsStr = "Other laboratories"
const cNextCladeGood = "good"
const cNextCladeMedium = "mediocre"
const cNextCladeBad = "bad"
const cNextCladeGoodMissing= "good (<10% Ns)"
const cNextCladeMediumMissing = "mediocre"
const cNextCladeBadMissing = "bad (>100% Ns)"
const cMinProjectNameLength = 3
const cMaxProjectNameLength = 64
const cMaxProjectDescriptionLength = 256
const cMinPasswordLength = 8
const cMaxPasswordLength = 20

function isFullPeriod(periodStr)
{
    return periodStr == cFullPeriodStr
}
function getPeriodInDays( 
    periodStr
    )
{
    if ( periodStr==cLastMonthStr)
        return 30
    if ( periodStr ==cLastWeekStr)
        return 7
    if ( periodStr == cLastYearStr)
        return 365
    return 36500
}

function getOrganism(organism, shortName) {
  if (typeof organism !== 'string') {
    throw new TypeError('expected a string');
  }
  if (organism.length < 12)
    return organism.toString();

  if (shortName===true)
    return organism.substring(0,1)+ ". " +organism.split(' ')[1];
}

/**
 * @vuese
 * @param {string} name The request name
 */
function isReservedRequestName( name){
  /*if (typeof name !== 'string') {
      throw new TypeError('expected a string');
  }*/
    const reservedNames = ['All_samples', 'All_my_lab_samples']
    //return false
  if (name == null || typeof name === "undefined")
    return false
  return (reservedNames.findIndex(item => item === name) >= 0)
}

/**
 * @vuese
 * @param { string} columnName of a column containing [TABLE].[COLUMN NAME]
 * @returns { string} with only [COLUMN NAME]
 */
function extractColumnName(
  columnText
) {
  return columnText.split(".").pop()
}

/**
 * @vuese
 * @param { Array[ Array[ string]]} aMatrix array of values of each row of the matrix
 * @param { Array[ string]} columns array of columns name
 * @returns { Array[ Object]} for v-data-table
 */
function mapMatrixValuesToTableItems(
  aMatrix,
  columns
) {
  let map = []
  for (let i = 0; i < aMatrix.length; i++) 
  {
    var row = aMatrix[i]
    var rObj = {}
    for (let j = 0; j < columns.length; j++)
    {
      let id = extractColumnName( columns[ j])
      rObj[id] = row[j]
    }
    map.push(rObj)
  }
  return map
}

/**
 * @vuese
 * @param { Array[ string]} aColumns of the matrix
 * @returns { Array[ Object]} for v-data-table
 */
function mapStringArrayToTableHeaders(
  aColumns
  ) 
{
  let map = []
  for (let i = 0; i < aColumns.length; i++) 
  {
    var rObj = {}
    let item = aColumns[ i]
    let id = extractColumnName( item)
    rObj["value"] = id
    rObj["text"] = id.replaceAll( "_", " ")
    //rObj["align"] = 'center'
    map.push(rObj)
  }
  return map
  //return aColumns.map(item => ({ "value": item, "text": item }))
}

function extractValuesFromResults(aRows, index) 
{
  let aValues = []
  for (let i = 0; i < aRows.length; i++) 
    aValues.push( aRows[i][index])
  return aValues
}

function extractIntValuesFromResults(aRows, index) {
  let aValues = []
  for (let i = 0; i < aRows.length; i++)
    aValues.push(parseInt( aRows[i][index]))
  return aValues
}

function getProjectNameRules()
{
  var format = "\"'(){};, ";

  return [
    v => !!v || 'Name is required',
    v => (v && v.length <= cMaxProjectNameLength) || 'Name must be less than '+ cMaxProjectNameLength + ' characters',
    v => (v && v.length > cMinProjectNameLength) || 'Name must be more than '+ cMinProjectNameLength + ' characters',
    v => !format.split('').some(char =>v.includes(char)) ||
      'Name can not include forbidden characters "' + format + '"( including space)'
  ]
}

function getPasswordRules() {
  return [
    v => !!v || 'Password is required',
    v => (v && v.length <= cMaxPasswordLength) || 'Password must be less than ' + cMaxPasswordLength + ' characters',
    v => (v && v.length > cMinPasswordLength) || 'Password must be more than ' + cMinPasswordLength + ' characters',
  ]
}

function getProjectDescriptionRules() {
  return [
    v => ( v.length <= cMaxProjectDescriptionLength) || 'Decription must be less than 255 characters',
  ]
}

function extractMaxSubmissionDateFromProjectResults(requestResults) {
  if (requestResults == null || requestResults.headers == null)
    return []

  let dateIndex = requestResults.headers.findIndex(item => item === "SUBMISSION.Batch_submission_date");
  if (dateIndex < 0)
    return null

  let maxDateStr = ""
  let maxDate = 0
  let size = requestResults.rows.length
  for (let j = 0; j < size; j++) {
    let dateStr = requestResults.rows[j][dateIndex]
    if (dateStr !="")
    {
      if (maxDateStr =="")
      {
        maxDateStr = dateStr
        maxDate = Date.parse( maxDateStr)
      }  
      else{
        let submissionDate = Date.parse(dateStr)
        if ( submissionDate> maxDate)
        {
          maxDateStr = dateStr
          maxDate = Date.parse(maxDateStr)
        }
      }
    }
  }
  
  return maxDateStr
}

function extractSubmissionCountFromProjectResults(requestResults) {
  if (requestResults == null || requestResults.headers == null)
    return 0

  let countIndex = requestResults.headers.findIndex(item => item === "COUNT()");
  if (countIndex < 0)
    return 0

  let count = 0
  let size = requestResults.rows.length
  for (let j = 0; j < size; j++) {
    count += parseInt( requestResults.rows[j][countIndex])
  }
  return count
}

function extractOrganismFromProjectResults(requestResults, exitAtFirst) {
  if (requestResults == null || requestResults.headers == null)
    return []

  let organismsIndex = requestResults.headers.findIndex(item => item === "ISOLATE.Species_name")
  if (organismsIndex < 0)
    return []

  var lookup = {}
  var aNames = []
  let size = requestResults.rows.length
  for (let j = 0; j < size; j++) {
    var name = requestResults.rows[j][organismsIndex];
    if (!(name in lookup)) {
      lookup[name] = 1;
      aNames.push(name);
      if (exitAtFirst)
        break
    }
  }
  return aNames
}

function getLaboratoryName(user)
{
  return user.laboratoryName
}

function getLaboratoryShortName(user) {
  return user.laboratoryShortName
}
function getLaboratoryId(user) {
  return user.laboratoryId
}

function setUserRole(user, userRole) 
{
  user.userRole = userRole
}

function setLaboratoryId(user, laboratoryId) {
  user.laboratoryId = laboratoryId
}
function setLaboratoryShortName(user, laboratoryName) {
  user.laboratoryShortName = laboratoryName
}
function setLaboratoryName(user, laboratoryName) {
  user.laboratoryName = laboratoryName
}
function getUserRole(user) {
  return user.userRole
}

function getUsername(user) {
  return user.preferred_username
}

function isValidUser( user)
{
  if (user==null)
    return false
  return ( this.getUserRole( user) != null && this.getLaboratoryId( user) != null)
}
function getUserMail(user) {
  return user.email
}
function getSubSetForPeriod(
    aDateValue,
    updatePeriod
    )
{
    if (isFullPeriod(updatePeriod))
        return aDateValue

    var aKeys = Misc.getKeys(aDateValue)
    let size = aKeys.length
    let startingIndex = -1
    let currentDate = MyDate.getCurrentDate()
    let periodInDays = this.getPeriodInDays(updatePeriod)
    for (let i = size - 1; i >= 0; i--)
    {
      if (MyDate.getDiffDateInDays(aKeys[i], currentDate) <= periodInDays)
            startingIndex = i
        else
            break
    }
    var aSubSet = {}
    var aValues = Misc.getValues(aDateValue)
    for (let i = startingIndex; i < size; i++)
        aSubSet[aKeys[i]] = aValues[i]
    return aSubSet
}

function updateCriteria(criteria, user)
{
  if (criteria==null)
    return criteria
  return (criteria.replace('MY_LAB', "'" + this.getLaboratoryName(user) + "'"));
}

function isDebugMode() {
  if (APP_DEBUG == "true")
    return true;
  return false;
}

function getRequestByName( requests, name)
{
    for (let i = 0; i < requests.length; i++)
    {
        if (requests[i].name === name)
            return requests[i];
    }
    return null;
}
function exportDataToCSV( fileName, headers,  aRows ) 
{
  let csvContent = "";
  if( headers)
  {
    console.log( headers)
    csvContent += headers.join(",")+ "\n";
  }

  if (aRows) 
  {
      aRows.forEach((row) => {
        csvContent += row.join(",") + "\n";
      });

      // Creating a Blob for having a csv file format
      // and passing the data with type
      const blob = new Blob([csvContent], {
        type: "text/csv;charset=utf-8,",
      });


      // Creating an object for downloading url
      const url = window.URL.createObjectURL(blob);

      // Creating an anchor(a) tag of HTML
      const a = document.createElement("a");

      // Passing the blob downloading url
      a.setAttribute("href", url);

      // Setting the anchor tag attribute for downloading
      // and passing the download file name
      a.setAttribute("download", fileName+".csv");

      // Performing a download with click
      a.click();
    }
  }
      
  function logout(message, router, store)
  {
      console.log(message);
      store.dispatch(LOGOUT)
          .then(() => {router.push({ name: "home" }); router.replace({ path: '/home' }); router.go()})

  }

export default {
    cMaxProjectNameLength,
    cMaxProjectDescriptionLength,
    cLastMonthStr,
    cLastWeekStr,
    cLastYearStr,
    cFullPeriodStr,
    cUnknownStr,
    cEmptyLineage,
    cDataEmptyStr,
    cEmptyLabStr,
    cOtherLabsStr,
    cNextCladeGood,
    cNextCladeMedium,
    cNextCladeBad,
    cNextCladeGoodMissing,
    cNextCladeMediumMissing,
    cNextCladeBadMissing,
    updateCriteria,
    isFullPeriod,
    getPeriodInDays,
    getSubSetForPeriod,
    getOrganism,
    extractColumnName,
    extractOrganismFromProjectResults,
    extractMaxSubmissionDateFromProjectResults,
    extractSubmissionCountFromProjectResults,
    extractValuesFromResults,
    extractIntValuesFromResults,
    mapMatrixValuesToTableItems,
    mapStringArrayToTableHeaders,
    isReservedRequestName,
    getProjectNameRules,
    getProjectDescriptionRules,
    getPasswordRules,

    getUsername,
    getLaboratoryShortName,
    getLaboratoryId,
    getLaboratoryName,
    getUserRole,
    getUserMail,
    isValidUser,
    setUserRole,
    setLaboratoryId,
    setLaboratoryShortName,
    setLaboratoryName,

    getRequestByName,
    exportDataToCSV,
    logout,

    isDebugMode
}
   