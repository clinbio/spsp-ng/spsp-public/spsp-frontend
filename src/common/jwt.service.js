const ID_TOKEN_KEY = "id_token";
const ID_ACCESS_TOKEN_KEY = "access_token";

export const getToken = () => {
    return window.localStorage.getItem(ID_TOKEN_KEY);
};

export const saveToken = token => {
    window.localStorage.setItem(ID_TOKEN_KEY, token);
};

export const destroyToken = () => {
    window.localStorage.removeItem(ID_TOKEN_KEY);
};

export const getAccessToken = () => {
    return window.localStorage.getItem(ID_ACCESS_TOKEN_KEY);
};

export const saveAccessToken = token => {
    window.localStorage.setItem(ID_ACCESS_TOKEN_KEY, token);
};

export const destroyAccessToken = () => {
    window.localStorage.removeItem(ID_ACCESS_TOKEN_KEY);
};

export default { 
    getToken, 
    saveToken, 
    destroyToken,
    getAccessToken,
    saveAccessToken,
    destroyAccessToken
};
