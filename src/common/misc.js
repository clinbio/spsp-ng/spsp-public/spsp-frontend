function renameKey(obj, oldKey, newKey) {
  obj[newKey] = obj[oldKey];
  delete obj[oldKey];
}

function getFloatRegex()
{
  return new RegExp('^\\d*(\\.\\d+)?$')
}

function getIntRegex()
{
  return new RegExp('[0-9]+')
}

function getHexColorFromLiteral(colorStr)
{
  const div = document.createElement('div')
  div.style.color = colorStr
  document.body.appendChild(div)

  const computedColor = window.getComputedStyle(div).color
  const matchResult = computedColor.match(/\d+/g)
  if (matchResult !== null) {
    const colors = matchResult.map((a) => parseInt(a, 10))

    document.body.removeChild(div)
    return colors.length >= 3
      ? '#' + ((1 << 24) + (colors[0] << 16) + (colors[1] << 8) + colors[2]).toString(16).substr(1)
      : false
  }

  return false
}

function countByKey( array, key)
{
  if (!array[key])
    array[key] = 1
  else
    array[key] += 1
}

function countByTwoKey(array, key1, key2) {
    if (!array[key1]) {
      array[key1] = []
      array[key1].push({ count: 1, tag: key2 })
    }
    else {
      var obj = array[key1].find(o => o.tag == key2)
      if (!obj)
        array[key1].push({ count: 1, tag: key2 })
      else 
        obj.count+=1
    }
}

function countByThreeKey(array, key1, key2, key3) {
  if (!array[key1]) {
    array[key1] = []
    array[key1].push({ count: 1, first: key2, second: key3})
  }
  else {
    var obj = array[key1].find(o => o.first == key2 && o.second == key3)
    if (!obj)
      array[key1].push({ count: 1, first: key2, second: key3 })
    else
      obj.count += 1
  }
}

function completeCountByTwoKey(array, aSubKeys) 
{
  let subkeyCount = aSubKeys.length
  let aKeys = Object.keys(array)
  let size = aKeys.length
  for (let j = 0; j < size; j++)
  {
    let key = aKeys[j]
    for (let i = 0; i < subkeyCount; i++)
    {
      let subkey = aSubKeys[ i]
      var obj = array[key].find(o => o.tag == subkey)
      if (!obj)
        array[key].push({ count: 0, tag: subkey })
    }
  }
}

function extractCountValues(array, tag) {
  let aKeys = Object.keys(array)
  let aValues = {}
  let size = aKeys.length
  for (let i = 0; i < size; i++)
  {
    var obj = array[aKeys[i]].find(o => o.tag === tag)
    if (!_.isUndefined(obj))
      aValues[aKeys[i]] = obj.count
  }
  return aValues
}

function aggregateByKey(array, key, value)
{
    if (!array[key]) 
    {
        array[key] = []
        array[key].push(value)
    }
    else 
    {
        array[key].push(value)
    }
}


function sortByAttribute(array, attribute) {
  return array.sort(function (a, b) {
    var x = a[attribute]; var y = b[attribute];
    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
  });
}

function sortByKey(array)
{
  var aOrderedKey = Object.keys(array).sort().reduce(
    (obj, key) => {
      obj[key] = array[key];
      return obj
    },
    {}
    )
  return aOrderedKey
}

function sortByValue(array) {
  var ordered = Object.keys(array).sort(function (a, b) { return array[b] - array[a];}).reduce(
        (obj, key) => { obj[key] = array[key]; return obj;},
        {}
    )
  return ordered;
}

/*function compareKeyDate(a, b) {
   // Compare the 2 dates
  return ((Date(Object.keys(a)[0]) - Date(Object.keys(b)[0]))<0)
}*/

function average( aValues) 
{
  let mean = Number.NaN
    let size = aValues.length
  if (size <= 0)
    return mean

  return aValues.reduce(function (p, c, i, a) { return p + (c / a.length) }, 0);
}

function median(aValues){
  let aStats = {}
  let size = aValues.length
  if (size == 0)
  {
    aStats.median = Number.NaN
    aStats.min = Number.NaN
    aStats.max = Number.NaN
    return aStats
  }

  // Sort Array
  aValues = aValues.sort((a, b) => { return a - b; });

  // Array Length: Even
  if ((size % 2) == 0)
  {
    aStats.median = ((aValues[(size / 2) - 1] + aValues[size/2]))/2 // Mean of middle numver
  }
  else
  {
    aStats.median = aValues[(size - 1) / 2]; // Middle Number
  }
  aStats.min = aValues[0]
  aStats.max = aValues[size-1]
  return aStats
}

function getWordCount(text)
{
    if (text == null)
        return 0
    return text.split(' ').length
}

function checkFlag( value, flag)
{
  return ((value & flag) == flag)
}



function truncate(value, round) 
{
  //return Math.ceil(value * Math.pow(10, round))
  return +(Math.round(value + "e+" + round) + "e-" + round);
}

function getTooltipForKeyValue( aKeyValues, size, postFix)
{
  if (_.isEmpty(aKeyValues))
    return "-"
  var aKeys = getKeys(aKeyValues)
  var aValues = getValues(aKeyValues)
  let text = ""
  let minSize = size
  if (size<=0)
    minSize = aKeys.length
  else
    minSize = Math.min(aKeys.length, size)

  for (let i = 0; i < minSize; i++)
    text += aKeys[i] + " (" + aValues[i] + postFix + ")<br/>"
  return text
}

function getKeys(aKeyValues) 
{
  return Object.keys(aKeyValues)
}

function getValues(aKeyValues) {
  return Object.values(aKeyValues)
}

function applyKeyOrder( aObject, akeys)
{
  var aNewObject = []
  let keyCnt = akeys.length
  for (let i = 0; i < keyCnt; i++)
    aNewObject.push( aObject[ akeys[ i]])
  return aNewObject
}

function generateDayWiseTimeSeries(baseval, count, yrange) {
  var i = 0;
  var series = [];
  while (i < count) {
    var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

    series.push([baseval, y]);
    baseval += 86400000;
    i++;
  }
  return series;
}

function heatMap_generateData(count, yrange) {
  var i = 0;
  var series = [];
  while (i < count) {
    var x = (i + 1).toString();
    var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;
    series.push({
      x: x,
      y: y
    });
    i++;
  }
  return series;
}

//get any percentile from an array
function getPercentile(data, percentile) 
{
  data.sort(numSort);
  var index = (percentile / 100) * data.length;
  var result;
  if (Math.floor(index) == index) {
    result = (data[(index - 1)] + data[index]) / 2;
  }
  else {
    result = data[Math.floor(index)];
  }
  return result;
}
//because .sort() doesn't sort numbers correctly
function numSort(a, b) {
  return a - b;
}

export default {
    generateDayWiseTimeSeries,
    heatMap_generateData,
    
    getWordCount,
    renameKey,
    applyKeyOrder,
    getFloatRegex,
    getIntRegex,
    getHexColorFromLiteral,

    sortByKey,
    countByKey,
    countByTwoKey,
    countByThreeKey,
    completeCountByTwoKey,
    extractCountValues,
    aggregateByKey,
    
    sortByValue,
    sortByAttribute,
    getKeys,
    getValues,
    getTooltipForKeyValue,
    
    checkFlag,
    average,
    median,
    getPercentile,
    truncate
}