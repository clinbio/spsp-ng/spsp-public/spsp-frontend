import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
    },
    theme: {
        themes: {
            light: {
                primary: '#1aafd0', /*@color@*/
                secondary: '#b0bec5',
                accent: '#8c9eff',
                error: '#b71c1c',
                bg_primary: '#e2eff2',
                lo: '#ebeeee'
                },
        },
    },
});
