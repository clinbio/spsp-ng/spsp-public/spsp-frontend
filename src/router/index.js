import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import _ from 'lodash'
import { cUseKeyCloackIdentification } from "@/common/config";
import { SITE_ABOUT } from "@/common/config";
import { SITE_URL } from "@/common/config";

Vue.use(Router);

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next()
        return
    }

    if (!cUseKeyCloackIdentification)
        next('/login')
}

function dynamicPropsFn(route) {
    return {
        name: route.params.name,
        group: route.params.group
    }
}


const routes = [
    {
        path: "/",
        name: 'home',
        component: () => import("@/views/Home"),
        beforeEnter: ifAuthenticated,
        meta: {},

    },
    /*{
        path: '/login',
        name: 'login',
        component: () => import("@/views/Login"),
        beforeEnter: ifNotAuthenticated
    },*/
    {
        path: '/permission-denied',
        name: 'permissionDenied',
        component: () => import("@/views/PermissionDenied")
    },
    {
        path: '/uploads',
        name: 'uploads',
        component: () => import("@/views/Uploads"),
        beforeEnter: ifAuthenticated,
        meta: {}
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import("@/views/MyDashboard"),
        beforeEnter: ifAuthenticated,
        meta: {}
    },
    /*{
        path: '/qc-reviews',
        name: 'qc-reviews',
        component: () => import("@/views/SamplesQCReview"),
        beforeEnter: ifAuthenticated,
        meta: {
            role: []
        }
    },*/
    {
        path: '/request/:name',
        beforeEnter: ifAuthenticated,
        props: dynamicPropsFn,
        component: () => import("@/views/Request"),
        children: [
            {
                path: 'summary',
                name: 'request-summary',
                component: () => import("@/components/projects/RequestSummary"),
                props: dynamicPropsFn,
                meta: {
                    //role: ['EDITOR']
                }
            },
            {
                path: 'edit',
                name: 'request-edit',
                component: () => import("@/components/projects/RequestEdit"),
                props: dynamicPropsFn,
                meta: {
                    role: ['SPSPEditor', 'SPSPManager']
                }
            }
        ]    
    },
    {
        path: '/projects/input/create',
        name: 'project-input-create',
        component: () => import("@/views/RequestAdd"),
        beforeEnter: ifAuthenticated,
        meta: {
            role: ['SPSPEditor', 'SPSPManager']
        }
    },
    {
        path: "*",
        name: 'not-found',
        component: () => import("@/views/NotFound"),
        beforeEnter: ifAuthenticated,
        meta: {}
    },
    {
        path: '/page-test',
        name: 'page-test',
        component: () => import("@/views/Test/PageTest"),
        beforeEnter: ifAuthenticated,
        meta: {}
    },
    {
        path: '/about',
        name: 'about',
        component: () => SPSP_About,
        beforeEnter() {
            window.open(SITE_ABOUT, '_blank');  location.href = ' ' },
        meta: {}
    },
    {
        path: '/documentation',
        name: 'documentation',
        beforeEnter() {
            window.open(SITE_URL+"documentation\\SPSP_user_documentation.pdf", '_blank'); location.href = ''
        },
        meta: {}
    },
];

const router = new Router({
    routes: routes,
    mode: "history"
});

router.beforeEach((to, from, next) => {

    if (cUseKeyCloackIdentification)
    {
        if (to.meta.role) 
        {
            let checkRoleMeta = false;

            if (to.meta.role.length!=0 || to.meta.role.indexOf(store.getters.currentRole) >= 0) {
                checkRoleMeta = true;
            }

            if (checkRoleMeta) {
                next();
            } else {
                next({ name: 'permissionDenied' });
            }
        } 
        else 
        {
            next();
        }

    }
    else
    {
        if (store.getters.currentUser.firstLogin) {
            next({ name: 'profile' })
        }

        if (to.meta.role && to.meta.functions) 
        {
            let checkRole = false;
            var checkFunction = false;

            //roleExist = true;
            if (!to.meta.role.length || to.meta.role.indexOf(store.getters.currentRole) >= 0) 
            {
                checkRole = true;
            }

            if (!to.meta.functions.length || _.intersection(to.meta.functions, store.getters.currentFunctions).length) 
            {
                checkFunction = true;
            }

            if (checkRole && checkFunction) {
                next();
            } else {
                next({ name: 'permissionDenied' });
            }
        } else 
        {
            next();
        }
    }
});

export default router;
