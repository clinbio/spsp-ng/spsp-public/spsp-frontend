# SPSP User Interface

## Description
The SPSP User Interface is the frontend that presents the data to SPSP users, from where they can browse and visualize results.

## Authors
Main developer: Gérard Bouchet

## License
The source code is licensed under GPL-3.0-or-later and available via GitLab.
For more information or any inquiries, please reach out to legal@sib.swiss.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
