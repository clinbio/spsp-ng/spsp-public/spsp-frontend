const { defineConfig } = require('@vue/cli-service')

//const GitRevisionPlugin = require('./node-modules/git-revision-webpack-plugin');
process.env.VUE_APP_VERSION = require('./package.json').version

module.exports = defineConfig({
  //publicPath: process.env.PUBLIC_URL || "/",
  pages: {
    // This is our main page which refers to the main Javascript file
    index: "src/main.js", 
    // Next, we list all other pages in the application, 
    // and let all of them refer to the main Javascript file as well.
    other_page: "src/main.js",
  },
  outputDir: process.env.OUTPUT_DIR,
  devServer: {
    //logLevel: 'debug',
    proxy: {
      '^/SPSPPortal': {        
        target: 'https://spsp.sib.swiss',
        logLevel: 'debug',
        changeOrigin: true,    
      },
      '^/DEVPortal': {
        target: 'https://spspweb.vital-it.ch',
        logLevel: 'debug',
        changeOrigin: true,
      }
    }
  },
  runtimeCompiler: true,
  transpileDependencies: [
    'vuetify'
  ],
  /*configureWebpack: {
    plugins: [
      new GitRevisionPlugin()
    ]
    
  }*/
});
